<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'f0f271f65c279b8e3706dbec2b3add68f849988b',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'f0f271f65c279b8e3706dbec2b3add68f849988b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kartik-v/bootstrap-fileinput' => array(
            'pretty_version' => 'v5.5.4',
            'version' => '5.5.4.0',
            'reference' => '8de1bed638823c70272b2578847e2b31e42677ba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kartik-v/bootstrap-fileinput',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-krajee-base' => array(
            'pretty_version' => 'v3.0.5',
            'version' => '3.0.5.0',
            'reference' => '5c095126d1be47e0bb1f92779b7dc099f6feae31',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-krajee-base',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kartik-v/yii2-widget-fileinput' => array(
            'pretty_version' => 'v1.1.1',
            'version' => '1.1.1.0',
            'reference' => 'b5500b6855526837154694c2afab8dbc3afc8abd',
            'type' => 'yii2-extension',
            'install_path' => __DIR__ . '/../kartik-v/yii2-widget-fileinput',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
