<?php

namespace app\controllers;

use Yii;
use app\models\modelobandas;
use app\models\bandasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile; // Importa también esta clase si aún no lo has hecho

/**
 * BandasController implements the CRUD actions for modelobandas model.
 */
class BandasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all modelobandas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new bandasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single modelobandas model.
     * @param int $codigobanda Codigobanda
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigobanda)
    {
        $model = $this->findModel($codigobanda);

        if ($this->request->isPost && $model->load($this->request->post())) {
            Yii::info('Datos recibidos del formulario: ' . print_r($this->request->post(), true), __METHOD__);

            // Añadir la nueva valoración a las existentes
            $nuevaValoracion = $model->valoraciones;
            $model->valoraciones = ($model->getOldAttribute('valoraciones') ? $model->getOldAttribute('valoraciones') . "\n" : '') . $nuevaValoracion;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Valoraciones actualizadas correctamente.');
            } else {
                Yii::error('Error al guardar el modelo: ' . print_r($model->getErrors(), true), __METHOD__);
                Yii::$app->session->setFlash('error', 'Error al actualizar las valoraciones.');
            }
            return $this->redirect(['view', 'codigobanda' => $model->codigobanda]);
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new modelobandas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
public function actionCreate()
{
    $model = new modelobandas();

    if ($this->request->isPost) {
        if ($model->load($this->request->post())) {
            $imagen = UploadedFile::getInstance($model, 'file');
            if ($imagen !== null) {
                $ext = $imagen->getExtension();
                $model->imagen = Yii::$app->security->generateRandomString() . ".{$ext}";
                $path = Yii::getAlias('@webroot/uploads/') . $model->imagen;
                if ($imagen->saveAs($path) && $model->save()) {
                    // El modelo y la imagen se guardaron correctamente
                    return $this->redirect(['view', 'codigobanda' => $model->codigobanda]);
                } else {
                    // Hubo un error al guardar la imagen o el modelo
                    Yii::$app->session->setFlash('error', 'Hubo un error al guardar la imagen.');
                }
            }
        }
    }

    return $this->render('create', [
        'model' => $model,
    ]);
}


    /**
     * Updates an existing modelobandas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigobanda Codigobanda
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigobanda)
    {
        $model = $this->findModel($codigobanda);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigobanda' => $model->codigobanda]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing modelobandas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigobanda Codigobanda
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigobanda)
    {
        $this->findModel($codigobanda)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the modelobandas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigobanda Codigobanda
     * @return modelobandas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigobanda)
    {
        if (($model = modelobandas::findOne(['codigobanda' => $codigobanda])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
