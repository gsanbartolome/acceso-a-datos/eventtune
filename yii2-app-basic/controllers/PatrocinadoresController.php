<?php

namespace app\Controllers;

use app\models\Patrocinadores;
use app\models\PatrocinadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PatrocinadoresController implements the CRUD actions for Patrocinadores model.
 */
class PatrocinadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Patrocinadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PatrocinadoresSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patrocinadores model.
     * @param int $idpatrocinador Idpatrocinador
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idpatrocinador)
    {
        return $this->render('view', [
            'model' => $this->findModel($idpatrocinador),
        ]);
    }

    /**
     * Creates a new Patrocinadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Patrocinadores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idpatrocinador' => $model->idpatrocinador]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patrocinadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idpatrocinador Idpatrocinador
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idpatrocinador)
    {
        $model = $this->findModel($idpatrocinador);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idpatrocinador' => $model->idpatrocinador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patrocinadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idpatrocinador Idpatrocinador
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idpatrocinador)
    {
        $this->findModel($idpatrocinador)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Patrocinadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idpatrocinador Idpatrocinador
     * @return Patrocinadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idpatrocinador)
    {
        if (($model = Patrocinadores::findOne(['idpatrocinador' => $idpatrocinador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
