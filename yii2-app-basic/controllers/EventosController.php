<?php

namespace app\controllers;

use Yii;
use app\models\modeloeventos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\eventosSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile; // Importa también esta clase si aún no lo has hecho

/**
 * EventosController implements the CRUD actions for modeloeventos model.
 */
class EventosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all modeloeventos models.
     *
     * @return string
     */
    public function actionIndex()
    {
                $searchModel = new eventosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single modeloeventos model.
     * @param int $codigoevento Codigoevento
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoevento)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoevento),
        ]);
    }

    /**
     * Creates a new modeloeventos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new modeloeventos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $imagen = UploadedFile::getInstance($model, 'file');
                if ($imagen !== null) {
                    $ext = $imagen->getExtension();
                    $model->imagen = Yii::$app->security->generateRandomString() . ".{$ext}";
                    $path = Yii::getAlias('@webroot/uploads/') . $model->imagen;
                    if ($imagen->saveAs($path) && $model->save()) {
                        // El modelo y la imagen se guardaron correctamente
                        return $this->redirect(['view', 'codigoevento' => $model->codigoevento]);
                    } else {
                        // Hubo un error al guardar la imagen o el modelo
                        Yii::$app->session->setFlash('error', 'Hubo un error al guardar la imagen.');
                    }
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing modeloeventos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoevento Codigoevento
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoevento)
    {
        $model = $this->findModel($codigoevento);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $imagen = UploadedFile::getInstance($model, 'file');
                if ($imagen !== null) {
                    $ext = $imagen->getExtension();
                    $model->imagen = Yii::$app->security->generateRandomString() . ".{$ext}";
                    $path = Yii::getAlias('@webroot/uploads/') . $model->imagen;
                    if ($imagen->saveAs($path) && $model->save()) {
                        // El modelo y la imagen se guardaron correctamente
                        return $this->redirect(['view', 'codigoevento' => $model->codigoevento]);
                    } else {
                        // Hubo un error al guardar la imagen o el modelo
                        Yii::$app->session->setFlash('error', 'Hubo un error al guardar la imagen.');
                    }
                } else {
                    if ($model->save()) {
                        return $this->redirect(['view', 'codigoevento' => $model->codigoevento]);
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing modeloeventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoevento Codigoevento
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoevento)
    {
        $this->findModel($codigoevento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the modeloeventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoevento Codigoevento
     * @return modeloeventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoevento)
    {
        if (($model = modeloeventos::findOne(['codigoevento' => $codigoevento])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionEliminarTodasLasConsultas()
{
    // Aquí implementa la lógica para eliminar todas las consultas
    // Por ejemplo:
    \app\models\modeloeventos::deleteAll();

    // Redireccionar a la página index después de eliminar las consultas
    return $this->redirect(['index']);
}

}
