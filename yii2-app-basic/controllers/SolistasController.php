<?php

namespace app\controllers;

use Yii;
use app\models\modelosolistas;
use yii\data\ActiveDataProvider;
use app\models\solistasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;




/**
 * SolistasController implements the CRUD actions for modelosolistas model.
 */
class SolistasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all modelosolistas models.
     *
     * @return string
     */
public function actionIndex()
{
    $searchModel = new solistasSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
}

    /**
     * Displays a single modelosolistas model.
     * @param int $codigosolista Codigosolista
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigosolista)
    {
        $model = $this->findModel($codigosolista);

        if ($this->request->isPost && $model->load($this->request->post())) {
            Yii::info('Datos recibidos del formulario: ' . print_r($this->request->post(), true), __METHOD__);

            // Añadir la nueva valoración a las existentes
            $nuevaValoracion = $model->valoraciones;
            $model->valoraciones = ($model->getOldAttribute('valoraciones') ? $model->getOldAttribute('valoraciones') . "\n" : '') . $nuevaValoracion;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Valoraciones actualizadas correctamente.');
            } else {
                Yii::error('Error al guardar el modelo: ' . print_r($model->getErrors(), true), __METHOD__);
                Yii::$app->session->setFlash('error', 'Error al actualizar las valoraciones.');
            }
            return $this->redirect(['view', 'codigosolista' => $model->codigosolista]);
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new modelosolistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new modelosolistas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $imagen = UploadedFile::getInstance($model, 'file');
                if ($imagen !== null) {
                    $ext = $imagen->getExtension();
                    $model->imagen = Yii::$app->security->generateRandomString() . ".{$ext}";
                    $path = Yii::getAlias('@webroot/uploads/') . $model->imagen;
                    if ($imagen->saveAs($path) && $model->save()) {
                        // El modelo y la imagen se guardaron correctamente
                        return $this->redirect(['view', 'codigosolista' => $model->codigosolista]);
                    } else {
                        // Hubo un error al guardar la imagen o el modelo
                        Yii::$app->session->setFlash('error', 'Hubo un error al guardar la imagen.');
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing modelosolistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigosolista Codigosolista
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigosolista)
    {
        $model = $this->findModel($codigosolista);

        if ($this->request->isPost && $model->load($this->request->post())) {
            $imagen = UploadedFile::getInstance($model, 'file');
            if ($imagen !== null) {
                $ext = $imagen->getExtension();
                $model->imagen = Yii::$app->security->generateRandomString() . ".{$ext}";
                $path = Yii::getAlias('@webroot/uploads/') . $model->imagen;
                if ($imagen->saveAs($path)) {
                    // Imagen guardada correctamente
                } else {
                    // Hubo un error al guardar la imagen
                    Yii::$app->session->setFlash('error', 'Hubo un error al guardar la imagen.');
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'codigosolista' => $model->codigosolista]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing modelosolistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigosolista Codigosolista
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigosolista)
    {
        $this->findModel($codigosolista)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the modelosolistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigosolista Codigosolista
     * @return modelosolistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigosolista)
    {
        if (($model = modelosolistas::findOne(['codigosolista' => $codigosolista])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
