<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    body, .wrapper {
        background-image: url('/eventtune/yii2-app-basic/assets/imagenes/bannerfinal1.png');
        background-size: cover;
    }
    
    body {
        background-color: white;
    }

    .wrapper {
        background-color: white;
    }
</style>

<div class="site-contact" style="background-color: white;">
    <div class="red-bar-left" style="position: absolute; top: 35%; left: 22%; width: 6px; height: 93%; background-color: red;"></div>
    <div class="red-bar-left" style="position: absolute; top: 45%; left: 17%; width: 6px; height: 70%; background-color: red;"></div>
    <div class="red-bar-left" style="position: absolute; top: 35%; right: 22%; width: 6px; height: 93%; background-color: red;"></div>
    <div class="red-bar-left" style="position: absolute; top: 45%; right: 17%; width: 6px; height: 70%; background-color: red;"></div>

    <div class="red-blurred-bg" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); z-index: -3;"></div>
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card shadow-lg border-0">
                    <div class="card-body">
                        <h1 class="card-title" style="font-size: 46px; text-align: center; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><?= Html::encode($this->title) ?></h1>
                        <p class="card-text" style="font-size: 22px; text-align: center; font-family: 'Times New Roman', Times, serif;">
                            Estamos aquí para ayudarte. Si tienes alguna pregunta, problema o simplemente quieres contactarnos, por favor completa el siguiente formulario. Gracias por tu interés.
                        </p>
                        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

                            <div class="alert alert-success" role="alert">
                                Gracias por contactarnos. Te responderemos lo antes posible.
                            </div>

                            <p>
                                Ten en cuenta que si activas el depurador de Yii, deberías poder ver el mensaje de correo en el panel de correo del depurador.
                                <?php if (Yii::$app->mailer->useFileTransport): ?>
                                    Debido a que la aplicación está en modo de desarrollo, el correo electrónico no se envía, sino que se guarda como un archivo en <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                                    Por favor, configura la propiedad <code>useFileTransport</code> del componente de aplicación <code>mail</code> en falso para habilitar el envío de correos electrónicos.
                                <?php endif; ?>
                            </p>

                        <?php else: ?>

                            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                            <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Nombre') ?>

                            <?= $form->field($model, 'email')->label('Correo electrónico') ?>

                            <?= $form->field($model, 'subject')->label('Asunto') ?>

                            <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Mensaje')->error() ?>

                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-md-3">{image}</div><div class="col-md-6">{input}</div></div>',
                            ])->label('Código de verificación') ?>

                            <div class="form-group">
                                <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>
