<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Yii Application</title>

    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <style>
        body {
            overflow-x: hidden;
            margin: 0; /* Asegura que no haya margen en el cuerpo */
            padding: 0; /* Asegura que no haya relleno en el cuerpo */
            background: url('/eventtune/yii2-app-basic/assets/imagenes/bannerfinal4.jpg') no-repeat center center fixed; /* Utiliza la URL de la imagen de fondo */
            background-size: cover;
        }

        /* Nuevo estilo para el fondo difuminado rojo */
        .red-blurred-bg {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); /* Cambia los valores de los colores y la opacidad según tus preferencias */
            z-index: -3; /* Coloca el fondo detrás de otros elementos */
        }

        /* Estilos restantes... */
        .container {
            display: flex;
            justify-content: center;
        }
        
        


.carousel-item img {
    width: 100%;
    height: 100vh;
    object-fit: cover;           
}

.body-content {
    position: relative;
}

.row {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 20px;
    margin-bottom: 20px;
}

.col-lg-4 {
    flex: 1;
    position: relative;
    animation: fadeInUp 1s ease-out;
    margin: 0 10px;
}

.text-box {
    background-color: rgba(255, 255, 255, 0.8);
    margin-bottom: 20px;
    padding: 20px;
    position: relative;
    overflow: hidden;
    text-align: center;
    color: white;
}

.text-box2 {
    background-color: rgba(255, 255, 255, 0.8);
    margin-bottom: 20px;
    padding: 20px;
    position: relative;
    overflow: hidden;
    text-align: center;
    color: black;
    margin-right: -50px;
}

.circle {
    border-radius: 50%;
    background-color: #ff9999;
    padding: 40px;
    height: 250%;
    width: 350px;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-left: 57px; /* Ajustar hacia la derecha */
}

.circle .symbol {
    font-size: 24px;
    margin-right: 10px;
}

.circle h2 {
    margin-top: 10px; /* Ajustar el margen superior */
}

.circle p {
    margin-bottom: 10px; /* Ajustar el margen inferior */
}

.circle .btn-red {
    margin-top: 10px; /* Ajustar el margen superior */
}

.text-box a.btn-red,
.text-box2 button.btn-red {
    background-color: #3ea5fd;
    color: white;
    border: none;
    padding: 10px 20px;
    margin-top: 10px;
    cursor: pointer;
    text-decoration: none;
    transition: background-color 0.3s ease, transform 0.3s ease, box-shadow 0.3s ease;
    display: inline-block;
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1); /* Sombra inicial */
    font-weight: bold; /* Para que el texto sea un poco más grueso */
}

.text-box a.btn-red:hover,
.text-box2 button.btn-red:hover {
    background-color: #63baff;
    transform: scale(1.05);
    box-shadow: 0 8px 12px rgba(0, 0, 0, 0.2); /* Sombra al hacer hover */
}

.text-box a.btn-red,
.text-box2 button.btn-red {
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2); /* Sombra para el texto */
}
.big-container {
    display: flex;
    justify-content: space-between;
    position: relative;
}

.big-text {
    width: 50%;
    margin-left: 20px;
    margin-top: 20px;
    font-size: 20px;
    font-family: 'Serif', serif;
}


.extra-image img {
 
    max-width: 86.3%;
    width: 100%;
   margin-left: 105px;
}

.pink-rectangle {
    position: relative;
    background-color: #ff9999;
    width: calc(100% + 20px); /* Ancho extendido para salir del contenedor */
    height: 330px; /* Altura del cuadrado */
    left: -20px; /* Ajustar posición izquierda para centrarlo */
    z-index: -1; /* Detrás de otros elementos */
    margin-top: -285px; /* Ajustar el margen */
        box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.5); /* Efecto de sombra */

}

.custom-text {
    text-align: center;
    font-size: 24px;
    font-weight: bold;
    font-family: Arial, sans-serif;
    color: white; 
    margin-top: var(--text-margin-top, 0); /* Ajustar el margen superior */
}

.section-container {
    position: relative;
    margin-top: var(--section-margin-top, 0); /* Ajustar el margen superior */
}

.red-line {
    border-left: 5px solid red;
    height: 200px; 
    position: absolute;
}

.frame-outer {
    border: 4px solid red; /* Primer marco */
    padding: 10px; /* Separación del primer marco */
}

.frame-inner {
    border: 4px solid red; /* Segundo marco */
    padding: 10px; /* Separación del segundo marco */
}

.responsive-iframe {
    width: 100%;
    height: 315px;
    box-sizing: border-box; /* Incluir el padding en el ancho y alto total */
}

    .white-bg {
        position: absolute;
        background-color: white;
        height: 4000px;
        width: 86%;
        top: 60%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: -2; /* Coloca este elemento debajo de los elementos normales */
        box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.3); /* Aumenta la difuminación */

    }
    
    .white-bg2 {
        position: absolute;
        background-color: white;
        height: 1500px;
        width: 100%;
        top: 26.6%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: -2; /* Coloca este elemento debajo de los elementos normales */
        clip-path: polygon(0 0, 100% 0, 100% 60%, 50% 100%, 0 60%); /* Define la forma del clip-path */
    }
    
    .white-bg3 {
    position: absolute;
    background-color: white;
    height: 1000px;
    width: 100%;
    top: 82%;
    left: 50%;
    transform: translate(-50%, 0);
    z-index: -2;
    clip-path: polygon(0 100%, 100% 100%, 100% 40%, 50% 0, 0 40%); /* Define la forma del clip-path */
}



    </style>

</head>

<body>
    
    <!-- Fondo difuminado rojo -->
<div class="red-blurred-bg"></div>

<div class="site-index">
    
    
    <div class="site-index">

        <!-- Sección del Carrusel -->
<div id="carouselExample" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
                    
        <!-- Primer Slide -->
        <div class="carousel-item active" style="position: relative;">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/bannerfinal3.jpg" alt="Slide 1">
                    <div style="position: absolute; top: 39%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 48px;">
                <h1 style="font-size: 135px; font-weight: bold; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">EVENTUNE</h1>
            </div>
            <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
        </div>
        <!-- Segundo Slide -->
        <div class="carousel-item" style="position: relative;">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/bannerfinal1.png" alt="Slide 2">
            <div style="position: absolute; top: 42%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 48px;">
                <h1 style="font-size: 70px; font-weight: bold; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">
                    TODA LA MUSICA AL ALCANCE <br> DE TU MANO</h1>
            </div>
            <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
        </div>
        <!-- Tercer Slide -->
        <div class="carousel-item" style="position: relative;">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/bannerfinal4.jpg" alt="Slide 3">
            <div style="position: absolute; top: 39%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 48px;">
                <h1 style="font-size: 90px; font-weight: bold; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);">
                    DA EL SALTO</h1>
            </div>
            <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
        </div>
    </div>
</div>



        <!-- Fin de la Sección del Carrusel -->

        <!-- Sección del Contenido Principal -->
        <div class="body-content">

            <!-- Contenedor de la Sección -->
            <div class="section-container" style="--section-margin-top: -175px;">
                <div class="row">
                    <!-- Columna de Bandas -->
                    <div class="col-lg-4">
                        <div class="text-box circle">
                            <div class="symbol" style="font-size: 2em;">🎸</div>
                            <h2>BANDAS</h2>
                            <p>Anuncia a tu banda y consigue<br> tocar en donde más amas</p>
                            <a href="http://localhost/eventtune/yii2-app-basic/web/index.php/bandas/index" class="btn btn-red">Bandas</a>
                        </div>
                    </div>
                    <!-- Columna de Eventos -->
                    <div class="col-lg-4">
                        <div class="text-box circle">
                            <div class="symbol" style="font-size: 2em;">🎉</div>
                            <h2>EVENTOS</h2>
                            <p>Cualquier tipo de evento musical <br> es más que bienvenido en tu búsqueda <br> de bandas</p>
                            <a href="http://localhost/eventtune/yii2-app-basic/web/index.php/eventos/index" class="btn btn-red">Eventos</a>
                        </div>
                    </div>
                    <!-- Columna de Solistas -->
                    <div class="col-lg-4">
                        <div class="text-box circle">
                            <div class="symbol" style="font-size: 2em;">🎤</div>
                            <h2>SOLISTAS</h2>
                            <p>Si intentas abrirte paso como solista <br> no tienes más que anunciarte aquí</p>
                            <a href="http://localhost/eventtune/yii2-app-basic/web/index.php/solistas/index" class="btn btn-red">Solistas</a>
                        </div>
                    </div>
                </div>

                <!-- Texto Personalizado -->
                <div class="custom-text" style="--text-margin-top: -40px;">
                    Anuncia a tus banda o evento en Eventune, <br> donde la música y la comunidad se unen
                </div>
                <br>
                <div class="pink-rectangle"></div>
            </div>
            <br><br><br><br>
            
                <div class="white-bg"></div>
                <div class="white-bg2"></div>
                <div class="white-bg3"></div>



            <!-- Sección de Video y Descripción -->
            <div class="container">
                <div class="row">
                    <!-- Columna del Video -->
                    <div class="col-lg-6">
                        <div class="frame-outer">
                            <div class="frame-inner">
                                <iframe class="responsive-iframe" src="/eventtune/yii2-app-basic/assets/imagenes/0608.mp4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <!-- Columna de la Descripción -->
                    <div class="col-lg-6">
                        <div class="text-box2">
                            <h2>¡Descubre Eventune!</h2>
                            <br>
                            <p>Explora la plataforma que une a músicos, bandas y solistas de todo el mundo. Desde promocionar tus eventos hasta encontrar talento para tus proyectos musicales, Eventune es tu destino musical definitivo.</p>
                            <button class="btn btn-red dynamic-button" onclick="window.location.href='http://localhost/eventtune/yii2-app-basic/web/index.php/site/about';">Más información</button>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br><br>

            <!-- Sección de Imagen Extra -->
            <div class="extra-image">
                <img src="/eventtune/yii2-app-basic/assets/imagenes/filigrama_barra_final.png" alt="Tu Imagen">
            </div>
            <br><br><br>

            <!-- Sección de Contenido Informativo -->
            <div class="container">
                <!-- Imagen de Logotipo Grande -->
                <div style="position: relative; left: 70px; top: 180px;">
                    <img src="/eventtune/yii2-app-basic/assets/imagenes/stock1.png" alt="Imagen Grande" style="width: 500px; height: auto;">
                </div>
                
            <div class="container" style="position: relative; left: 0px; top: 0px;">
                <div class="big-text" style="text-align: left; margin-left: 350px; position: relative;"
                <!-- Líneas Rojas para Decoración -->
                <div class="red-line" style="left: -130px; top: 170px;"></div>
                <div class="red-line" style="left: 310px; top: 485px;"></div>
                
                </div>
                </div>

                <!-- Contenido de Texto -->
                <div class="big-text" style="text-align: left; margin-left: -110px;">
                    <h1 style="text-align: left; margin-bottom: 30px; font-size: 60px; position: relative; left: -50px;">¿Qué es Eventune?</h1>
                    <p style="text-align: left; padding-right: 130px;">
                        <br><br>
                        "Eventune es una vibrante plataforma digital diseñada para conectar apasionados músicos y talentosos solistas de todo el mundo. Nuestro objetivo es crear un espacio donde las bandas puedan promocionarse, encontrar oportunidades de presentación y colaborar con otros artistas en proyectos emocionantes.
                        <br><br>
                        ______________________________________
                        <br><br>
                    </p>
                        <div style="text-align: right; padding-right: 130px;">
                        Desde la búsqueda de nuevos miembros para tu banda hasta la organización de conciertos y eventos, Eventune ofrece una comunidad dinámica y colaborativa donde la música y la creatividad se fusionan. Únete a nosotros y descubre un mundo de posibilidades musicales en Eventune."
                        </div>
                    
                </div>
            </div>
            <br><br><br>

            <!-- Sección "¿Cómo Funciona?" -->
            <div class="container" style="position: relative;">
                <div class="big-text" style="text-align: left; margin-left: 40px;">
                    <h1 style="text-align: left; padding-right: 20px; margin-bottom: 20px; font-size: 60px; position: absolute; top: 50px; left: 20px;">¿Como Funciona?</h1>
                    <p style="text-align: left; padding-right: 80px;">
                        <br><br><br><br><br>
                        Eventune es una plataforma interactiva que conecta a músicos y solistas con oportunidades de presentación y colaboración de todas las formas posibles.
                        <br><br>
                        Los usuarios pueden crear perfiles de:
                        <br><br>
                        <span style="font-size: 12px;">🔴</span> Bandas<br><br>
                        <span style="font-size: 12px;">🔴</span> Eventos<br><br>
                        <span style="font-size: 12px;">🔴</span> Solistas<br><br>
                        <br>______________________________<br><br>
                        Promociona tus actuaciones y encontraras talento para proyectos musicales emocionantes. 
                        Nuestra comunidad dinámica y colaborativa es el lugar perfecto para descubrir nuevas oportunidades y conectar con otros amantes de la música
                    </p>
                </div>

                <!-- Imagen Adicional -->
                <div style="float: left; margin-top: 160px;">
                    <img src="/eventtune/yii2-app-basic/assets/imagenes/evento.jpg" alt="Imagen Grande" class="animated-img" style="width: 600px; height: 380px; border: 4px solid red;">
                </div>

                <div class="big-shadow2"></div>
            </div>

            <!-- Sección de Información Adicional -->
            <div class="container" style="position: relative; left: 100px; top: -275px;">
                <div class="big-text" style="text-align: left; margin-left: 350px; position: relative;">
                    <!-- Líneas Rojas para Decoración -->
                    <div class="red-line" style="left: 560px; top: 25px;"></div>
                    <div class="red-line" style="left: -540px; top: 35px;"></div>
                    <p style="text-align: right; padding-right: 20px;">
                        ______________________________________
                        <br><br>
                        Eventune es una vibrante plataforma digital diseñada para conectar apasionados músicos y talentosos solistas de todo el mundo. Nuestro objetivo es crear un espacio donde las bandas puedan promocionarse, encontrar oportunidades de presentación y colaborar con otros artistas en proyectos emocionantes.
                    </p>
                </div>
            </div>
            <!-- Fin de la Sección de Contenido Informativo -->
            
            <div style="margin-bottom: -150px;">
             <p style="text-align: center; position: relative; top: 96%; left: 0; font-family: 'Times New Roman', Times, serif; font-size: 27px; font-weight: bold;">
                   <i> "No existe música italiana, alemana o turca. Sólo existe la música."  </i>        
                                                <br>
                                      Giuseppe Verdi <br>
                                      (1813-1901)
                    
             </p>
             
                <div style="text-align: center; position: relative; top: 96%; left: 0;">
                    <img src="/eventtune/yii2-app-basic/assets/imagenes/logito.png" style="width: 8%;">
                </div>
        </div>                             

        </div>
        <!-- Fin de la Sección del Contenido Principal -->
   
    </div>
</div>


<div class="bottom-left-corner" style="position: relative; bottom: 20px; left: 10px; border-left: 5px solid red; border-bottom: 5px solid red; width: 130px; height: 275px; box-sizing: border-box;"></div>

<!-- Scripts de la Página -->
<script>
    $(document).ready(function() {
        $('.carousel').carousel(); // Inicializa el carrusel

        setInterval(function() {
            $('.carousel').carousel('next'); // Cambia al siguiente slide cada 5 segundos
        }, 5000);
    });
    
</script>

</body>

</html>