<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'EVENTUNE';
$this->params['breadcrumbs'][] = $this->title;

// Define los márgenes izquierdo y derecho de los textos
$textMarginLeft = '70px';
$textMarginRight = '-150px';

// Define el margen izquierdo de la imagen
$imageMarginLeft = '-200px';

// Nuevas variables para la posición horizontal del título y la imagen adicional
$titleHorizontalPosition = '10px'; // Ajusta la posición horizontal del título
$extraImageHorizontalPosition = '1040px'; // Ajusta la posición horizontal de la imagen adicional
$extraImageVerticalPosition = '80px'; // Ajusta la posición vertical de la imagen adicional
$extraImageSize = '120px'; // Ajusta el tamaño de la imagen adicional

$fotitoHeight = '66vh'; // Ajusta la altura de la imagen adicional
$fotitoVerticalPosition = '1100px'; // Ajusta la posición vertical de la imagen adicional

$overlayOpacity = '0.5'; // Opacidad del rectángulo negro (valor entre 0 y 1)
?>

<style>
    .site-about {
        font-size: 18px;
    }
    
    body {
        overflow-x: hidden;
    }

    .extra-image {
        position: absolute;
        top: <?= $extraImageVerticalPosition ?>; /* Ajusta la posición vertical de la imagen adicional */
        left: <?= $extraImageHorizontalPosition ?>; /* Ajusta la posición horizontal de la imagen adicional */
        width: <?= $extraImageSize ?>; /* Ajusta el tamaño de la imagen adicional */
    }

    .dynamic-content {
        display: flex;
        justify-content: center; /* Centra el contenido horizontalmente */
        align-items: center; /* Centra el contenido verticalmente */
        text-align: center; /* Centra el texto dentro del contenedor */
        max-width: 800px;
        padding: 40px 20px;
        position: relative; /* Agrega posición relativa para alinear la imagen adicional */
    }

    .dynamic-image {
        --image-width: 600px; /* Ancho de la imagen */
        --image-height: 800px; /* Alto de la imagen */
        width: var(--image-width); /* Ancho de la imagen */
        height: var(--image-height); /* Alto de la imagen */
        margin-right: 20px; /* Espacio entre la imagen y el texto */
        margin-left: <?= $imageMarginLeft ?>; /* Margen izquierdo de la imagen */
        border: 4px solid red; /* Agrega un borde negro */
        object-fit: cover; /* Para hacer las imágenes cuadradas */
    }

    .text-content {
        --text-margin-left: <?= $textMarginLeft ?>;
        --text-margin-right: <?= $textMarginRight ?>;
        max-width: 600px; /* Limita el ancho del texto */
        margin-left: var(--text-margin-left); /* Margen izquierdo del texto */
        margin-right: var(--text-margin-right); /* Margen derecho del texto */
    }

    .site-about {
        position: relative; /* Añade posición relativa al contenedor principal */
    }

    
    .fotito-container {
        position: absolute;
        width: 100vw;
        height: <?= $fotitoHeight ?>;
        top: <?= $fotitoVerticalPosition ?>;
        left: 50%;
        transform: translateX(-50%);
    }

    .fotito {
        width: 100%;
        height: 100%;
    }

    .overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: black;
        opacity: <?= $overlayOpacity ?>;
        z-index: 1;
    }
    
    .overlay-text {
        position: absolute;
        top: 50%; /* Centra verticalmente */
        left: 50%; /* Centra horizontalmente */
        transform: translate(-50%, -50%); /* Centra completamente */
        text-align: center;
        color: white; /* Color del texto */
        z-index: 2; /* Asegura que esté encima del overlay */
    }

    .overlay-text h2 {
        font-size: 86px; /* Tamaño de la fuente del título */
        margin-bottom: 10px; /* Espacio entre el título y el párrafo */
    }

    .overlay-text p {
        font-size: 24px; /* Tamaño de la fuente del párrafo */
        margin: 0; /* Elimina el margen por defecto */
    }

    .large-margin-space {
        margin-top: 360px; /* Espacio equivalente a 6x60px */
    }
    
    .accordion-container {
        position: relative;
        width: 80%; /* Ajusta el ancho del contenedor */
        height: 50vh; /* Ajusta la altura del contenedor */
        display: flex;
        overflow: hidden;
        margin: 0 auto; /* Centra horizontalmente el contenedor */
        margin-top: 20px; /* Espacio superior */
    }

    .accordion-item {
        flex: 1;
        overflow: hidden;
        position: relative;
        transition: flex 0.5s ease;
    }

    .accordion-item img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        transition: transform 0.5s ease;
    }

    .accordion-item .overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        transition: background-color 0.5s ease;
        display: flex;
        align-items: center;
        justify-content: center;
        opacity: 1;
    }

    .accordion-item .overlay-text {
        color: white;
        text-align: center;
        opacity: 0;
        transition: opacity 0.5s ease;
    }

    .accordion-item:hover {
        flex: 4;
    }

    .accordion-item:hover img {
        transform: scale(1.1);
    }

    .accordion-item:hover .overlay {
        background-color: rgba(0, 0, 0, 0);
    }

    .accordion-item:hover .overlay-text {
        opacity: 1;
    }
</style>

<div class="site-about">
    
    <div style=" margin-left: 390px;">
    <br><br>
<h1 class="title" style="font-family: 'Times New Roman', Times, serif; font-size: 72px; font-weight: bold; margin-left: -70px; color: black; text-shadow: 2px 2px 10px red;"><?= Html::encode($this->title) ?></h1>

    <!-- Agrega la imagen adicional -->
    <img src="/eventtune/yii2-app-basic/assets/imagenes/logito.png" alt="Extra Image" class="extra-image">
    
    <div class="dynamic-content">
        <!-- Imagen original -->
        <img src="/eventtune/yii2-app-basic/assets/imagenes/bannerfinal4.jpg" alt="Image 1" class="dynamic-image" id="original-image">
        
        <div class="text-content">
            <p>
                ¡Bienvenido a nuestra emocionante plataforma donde los eventos y las bandas se encuentran para crear experiencias inolvidables! Aquí, talentosos artistas se unen con apasionados organizadores de eventos, desbloqueando infinitas posibilidades para la música en vivo y la diversión.
            </p>
            
            <p>
                ¿Eres parte de una banda en busca de nuevos escenarios para compartir tu música con el mundo? ¿O quizás eres un organizador de eventos deseoso de ofrecer una experiencia única a tu audiencia? ¡Estás en el lugar adecuado! Nuestra plataforma está diseñada para facilitar la conexión entre talentosos músicos y emocionantes eventos.
            </p>

            <p>
                Explora una variedad de géneros musicales, desde el rock hasta el jazz, y encuentra el sonido perfecto para tu próximo evento. Ya sea un festival al aire libre, una fiesta en un club nocturno o un concierto íntimo, aquí encontrarás la banda ideal para hacer de tu evento un éxito rotundo.
            </p>

            <p>
                Prepárate para una experiencia única donde la música se fusiona con la emoción y la creatividad se encuentra en cada nota. Únete a nosotros y forma parte de una comunidad vibrante donde la pasión por la música y los eventos se encuentra en cada rincón de nuestra plataforma.
            </p>
        </div>
    </div>
        </div>


    <div class="fotito-container">
        <!-- Agrega los textos encima del overlay -->
        <div class="overlay-text white-text">
            <h2>EvenTune</h2>
            <p>Desde 2024, Uniendo Musicos y Amantes de la musica</p>
        </div>
        <img src="/eventtune/yii2-app-basic/assets/imagenes/stock6.jpg" alt="Fotito" class="fotito">
        <div class="overlay"></div>
    </div>
    
        <div style="margin-bottom: 720px;"></div>

<div class="red-line" style="position: absolute; width: 70%; height: 4px; background-color: red; top: 79%; left: 50%; transform: translate(-50%, -50%);"></div>

    
        <div class="accordion-container">
        <div class="accordion-item">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/stock2.png" alt="Image 1">
            <div class="overlay">
                <div class="overlay-text" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 24px; text-shadow: 4px 4px 8px rgba(0, 0, 0, 0.8);">
                    <h2>INNOVACION</h2>
                    <p>Abriendo la puerta al futuro de la musica</p>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/stock3.jpg" alt="Image 2">
            <div class="overlay">
                <div class="overlay-text" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 24px; text-shadow: 4px 4px 8px rgba(0, 0, 0, 0.8);">
                    <h2>MUSICA</h2>
                    <p>Compartiendo nuestro Amor Musical</p>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/stock4.jpg" alt="Image 3">
            <div class="overlay">
                <div class="overlay-text" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 24px; text-shadow: 4px 4px 8px rgba(0, 0, 0, 0.8);">
                    <h2>BANDAS</h2>
                    <p>Todas son Bienvenidas</p>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/stock5.webp" alt="Image 4">
            <div class="overlay">
                <div class="overlay-text" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); text-align: center; color: white; z-index: 2; font-family: 'Times New Roman', Times, serif; font-size: 24px; text-shadow: 4px 4px 8px rgba(0, 0, 0, 0.8);">
                    <h2>EVENTOS</h2>
                    <p>Descubre a un montón de artistas</p>
                </div>

            </div>
        </div>
    </div>
        
    <div class="red-line" style="position: absolute; width: 70%; height: 4px; background-color: red; top: 105%; left: 50%; transform: translate(-50%, -50%);"></div>
    <div class="red-line" style="position: absolute; width: 50%; height: 4px; background-color: red; top: 108%; left: 50%; transform: translate(-50%, -50%);"></div>


        <div style="margin-bottom: 300px;"></div>


</div>


