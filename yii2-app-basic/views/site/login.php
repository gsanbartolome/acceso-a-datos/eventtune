<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        .card-header {
            background-color: red !important;
        }
        .card-header h5 button {
            color: white !important;
        }
    </style>
    
        <style>
        .card-header {
            background-color: red !important;
        }
        .card-header h5 button {
            color: white !important;
        }
        h1 {
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
            font-size: 64px;
            text-align: center;
            color: #333;
            text-shadow: 2px 2px 4px #aaa;
        }
    </style>
</head>
<body>

<div class="container">
    <h1 class="mt-5 mb-4" style="font-family: 'Times New Roman', Times, serif; font-weight: bold; font-size: 54px;">Preguntas Frecuentes</h1>

    <div id="accordion">

        <!-- Pregunta 1 -->
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        ¿Cómo puedo registrarme en EvenTune?
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    Para registrarte en EvenTune, simplemente sigue estos pasos:
                    <ul>
                        <li>Dirígete a la página de Bandas, Solistas o Eventos</li>
                        <li>Rellena el formulario con tu información personal y de la banda, si aplica.</li>
                        <li>Confirma que todos los datos que has introducido son correctos</li>
                        <li>¡Listo! Ya puedes empezar a explorar EvenTune y todas sus funcionalidades.</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Pregunta 2 -->
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        ¿Cómo puedo buscar eventos en EvenTune?
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    En EvenTune, encontrar eventos es muy fácil:
                    <ul>
                        <li>Dirígete a la sección de Eventos.</li>
                        <li>Utiliza los filtros de búsqueda para refinar tus resultados por ubicación, género musical, fecha, etc.</li>
                        <li>Explora los eventos disponibles y encuentra el que más te interese.</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Pregunta 3 -->
        <div class="card">
            <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        ¿Puedo promocionar mi banda en EvenTune?
                    </button>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    ¡Por supuesto! En EvenTune, puedes promocionar tu banda de la siguiente manera:
                    <ul>
                        <li>Dirígete a la sección de Bandas.</li>
                        <li>Crea un perfil detallado de tu banda, incluyendo información, fotos y vídeos.</li>
                        <li>Explora las oportunidades de presentación y colaboración disponibles.</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Pregunta 4 -->
        <div class="card">
            <div class="card-header" id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                ¿Que hago si tengo un problema en EvenTune?                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                       Nuestro eficiente equipo siempre estara a tu servicio                    <ul>
                        <li>Ve a la seccion de contacto</li>
                        <li>Si no la encuentras tienes el acceso al fondo de la pagina</li>
                        <li>Introduce tus datos personales</li>
                        <li>Presentanos tu caso</li>
                        <li>¡Listo! Recibirás tus entradas por correo electrónico lo mas pronto posible</li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Pregunta 5 -->
        <div class="card">
            <div class="card-header" id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        ¿Qué debo hacer si tengo problemas técnicos con EvenTune?
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                <div class="card-body">
                    Si experimentas problemas técnicos con EvenTune, te recomendamos que hagas lo siguiente:
                    <ul>
                        <li>Verifica tu conexión a Internet.</li>
                        <li>Asegúrate de estar utilizando un navegador compatible y actualizado.</li>
                        <li>Intenta borrar la caché y las cookies de tu navegador.</li>
                        <li>Si el problema persiste, ponte en contacto con nuestro equipo de soporte técnico.</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
<br><br><br><br>

</body>
</html>
