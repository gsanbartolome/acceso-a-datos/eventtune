<?php
use app\assets\AppAsset;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        /* Estilos */
        header {
            background-color: #ff0000; 
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1); 
            height: 120px;    
            align-items: center;
            padding: 10px;
        }
        .navbar-brand {
            font-size: 2rem; 
            text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.8); 
            color: #ffffff; 
            font-weight: bold;
            margin-left: 15px; 
            display: flex;
            align-items: center; 
            margin-top: 10px;
        }
        .logo-img {
            max-width: 60px; 
            max-height: 60px; 
            margin-right: 10px; 
        }
        .navbar-nav {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%; 
        }
        .navbar-nav .nav-item {
            margin: 0 20px; 
        }
        .navbar-nav .nav-link {
            font-size: 1.2rem; 
            text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.8); 
            color: #ffffff !important; 
            transition: all 0.3s ease; 
        }
        .logout {
            text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.8); 
            color: #ffffff !important; 
            transition: all 0.3s ease; 
        }
        .navbar-nav .nav-link:hover, .logout:hover {
            transform: scale(1.1);
        }
        body {
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        main {
            flex: 1;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        .footer {
            padding: 40px;
            margin-top: 200px;
            box-shadow: 0px -4px 10px rgba(0, 0, 0, 0.1);
            position: relative; 
        }
        .footer-links ul {
            list-style: none;
            padding-left: 0;
        }
        .footer-links ul li {
            margin-bottom: 10px;
        }
        .footer-links ul li a {
            color: #fff;
        }
        .footer-text p {
            margin-bottom: 20px;
        }
        .footer-text p:last-child {
            margin-bottom: 0;
        }
        .footer-left,
        .footer-center,
        .footer-right {
            flex-basis: 30%; 
        }
        .footer-left img {
            max-width: 100%;
            height: auto;
            margin-bottom: 40px; 
        }
        .footer-left ul {
            list-style: none;
            padding: 0;
        }
        .footer-left ul li {
            margin-bottom: 30px;
        }
        .footer-left ul li a {
            color: #fff;
        }
        @media (max-width: 768px) {
            .footer .container {
                flex-direction: column;
                align-items: center;
            }
            .footer-left,
            .footer-center,
            .footer-right {
                flex-basis: 100%;
                margin-bottom: 20px;
                text-align: center;
            }
        }
        .footer-bottom {
            margin-top: 20px; 
        }
        @keyframes rotate {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .logo-img-footer.rotate {
            animation: rotate 0.5s ease forwards;
        }
        .music-controls {
            display: none;
            position: absolute;
            right: 10px;
            top: 60px;
            background-color: white;
            border: 1px solid #ccc;
            padding: 10px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            z-index: 1000;
        }
        .music-controls button, .music-controls input {
            display: block;
            margin: 10px 0;
        }
        .music-button {
            position: absolute;
            right: 10px;
            top: 10px;
            background-color: transparent;
            border: none;
            color: white;
            font-size: 20px;
            cursor: pointer;
        }
    </style>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<span style="font-size: 24px;"> <img src="/eventtune/yii2-app-basic/assets/imagenes/logito.png" alt="Logo" class="logo-img"> <span class="fas fa-star"></span> ' . Yii::$app->name . '</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' => Yii::$app->homeUrl],
            ['label' => 'Bandas', 'url' => ['/bandas/index']],
            ['label' => 'Eventos', 'url' => ['/eventos/index']],
            ['label' => 'Solistas', 'url' => ['/solistas/index']],
            ['label' => 'Eventune', 'url' => Url::to(['/site/about'])],
            [
                'label' => 'Opciones',
                'url' => '#',
                'items' => [
                    ['label' => 'Contactos', 'url' => ['/site/contact']],
                    ['label' => 'Preguntas', 'url' => ['/site/login']],
                ],
            ],
        ],
    ]);
    NavBar::end();
    ?>
    
    <button class="music-button" onclick="toggleMusicControls()">&#127925;</button>
    <div class="music-controls">
        <button id="music-toggle" onclick="toggleMusic()">Apagar Música</button>
        <label for="volume-slider">Volumen</label>
        <input type="range" id="volume-slider" min="0" max="1" step="0.1" value="0.1">
        <button onclick="nextTrack()">Siguiente Canción</button> <!-- Botón para la siguiente canción -->
    </div>
</header>

<main role="main" class="flex-shrink-0">
    <div style="text-align: left;">
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-5 text-light bg-dark" style="min-height: 557px; position: relative; box-shadow: 0px -2px 6px rgba(0, 0, 0, 0.3); overflow: hidden;">
    <div class="container d-flex justify-content-between flex-wrap" style="font-family: 'Times New Roman', Times, serif;">
        <div class="footer-left" style="text-align: center;">
            <img src="/eventtune/yii2-app-basic/assets/imagenes/logito.png" alt="Imagen" class="logo-img-footer" style="max-width: 230px; margin-left: -90px;">
            <ul style="margin-left: -90px;"> 
                <li><a href="#" style="font-size: 20px; font-weight: bold;">Politicas de la pagina</a></li>
                <li><a href="#" style="font-size: 20px; font-weight: bold;">Normas de uso</a></li>
                <li><a href="#" style="font-size: 20px; font-weight: bold;">Preguntas Frecuentes</a></li>
            </ul>
        </div>
        <div class="footer-center" style="text-align: center; margin-left: -90px; margin-top: 130px;">
            <h1 style="font-size: 76px; font-weight: bold; color: white;">EVENTUNE</h1>
            <h2 style="font-size: 20px; font-weight: bold; color: white;">De la música al mundo</h2>
        </div>
        <div class="footer-right" style="margin-right: -90px; margin-top: 20px; font-weight: bold; text-align: center;"> 
            <h3 style="font-size: 24px;">Contactanos</h3> 
            <br>
            <p style="font-size: 18px;">Calle Vargas 43. 39010 Santander, Cantabria</p> 
            <a href="mailto:EvenTuneForEver@gmail.com" style="font-size: 18px;">EvenTuneForEver@gmail.com</a> 
            <br>
            <br>
            <p style="font-size: 18px;">¿Tienes una consulta? Consulta las preguntas frecuentes</p>
            <br>
            <div style="text-align: center; margin: 0 auto;">
                <?= Html::a('Preguntas frecuentes', ['/site/login'], ['class' => 'btn btn-primary', 'style' => 'font-size: 14px; color: black; background-color: white;']) ?>
                <br><br>
                <?= Html::a('Contacto', ['/site/contact'], ['class' => 'btn btn-primary', 'style' => 'font-size: 14px; color: black; background-color: white;']) ?>
            </div>
        </div>
        <div class="footer-bottom w-100 text-center" style="border-top: 3px solid white; margin-top: 20px;">
            <p style="margin-top: 25px;">2024 - 2024 | EvenTune . Todos los derechos reservados | Diseño web por Guillermo San Bartolome</p>
        </div>
    </div>
    <div class="footer-rectangle" style="position: absolute; background-color: #555555; height: 50px; width: calc(100% + 40px); top: -25px; left: 739px; z-index: -1;"></div>
</footer>

<audio id="background-music" autoplay>
    <source src="/eventtune/yii2-app-basic/web/music/musica1.mp3" type="audio/mpeg">
    Tu navegador no soporta el elemento de audio.
</audio>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script>
function toggleMusicControls() {
    var controls = document.querySelector('.music-controls');
    controls.style.display = controls.style.display === 'block' ? 'none' : 'block';
}

// Lista de canciones
var tracks = [
    '/eventtune/yii2-app-basic/web/music/musica1.mp3',
    '/eventtune/yii2-app-basic/web/music/musica2.mp3',
    '/eventtune/yii2-app-basic/web/music/musica3.mp3'
];

var currentTrack = 0;

// Obtener el elemento de audio
var music = document.getElementById('background-music');
var musicToggle = document.getElementById('music-toggle');
var volumeSlider = document.getElementById('volume-slider');

// Inicializar el volumen al mínimo, pero no cero
var savedVolume = localStorage.getItem('musicVolume');
if (savedVolume !== null) {
    music.volume = savedVolume;
    volumeSlider.value = savedVolume;
} else {
    music.volume = 0.1;
    volumeSlider.value = 0.1;
}

// Función para apagar/encender la música
function toggleMusic() {
    if (music.paused) {
        music.play();
        musicToggle.textContent = 'Apagar Música';
    } else {
        music.pause();
        musicToggle.textContent = 'Encender Música';
    }
}

// Función para ajustar el volumen
volumeSlider.addEventListener('input', function() {
    music.volume = volumeSlider.value;
    localStorage.setItem('musicVolume', volumeSlider.value);
});

// Función para pasar a la siguiente canción
function nextTrack() {
    currentTrack++;
    if (currentTrack >= tracks.length) {
        currentTrack = 0;
    }
    music.src = tracks[currentTrack];
    music.play();
}

// Evento para cambiar a la siguiente canción cuando termine la actual
music.addEventListener('ended', nextTrack);

// Intentar reproducir música después de que la página se haya cargado
document.addEventListener('DOMContentLoaded', function() {
    music.play().catch(function(error) {
        console.log('La reproducción automática fue bloqueada.');
    });
});

$(document).ready(function(){
    $('.logo-img-footer').click(function(){
        $(this).addClass('rotate');
        setTimeout(function() {
            $('.logo-img-footer').removeClass('rotate');
        }, 500);
    });
});
</script>
