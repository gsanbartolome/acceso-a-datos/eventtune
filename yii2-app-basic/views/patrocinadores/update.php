<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Patrocinadores $model */

$this->title = 'Update Patrocinadores: ' . $model->idpatrocinador;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpatrocinador, 'url' => ['view', 'idpatrocinador' => $model->idpatrocinador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
