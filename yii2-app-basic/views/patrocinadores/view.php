<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Patrocinadores $model */

$this->title = $model->idpatrocinador;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patrocinadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idpatrocinador' => $model->idpatrocinador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idpatrocinador' => $model->idpatrocinador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idpatrocinador',
            'nombre',
        ],
    ]) ?>

</div>
