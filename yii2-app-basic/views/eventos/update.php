<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\modeloeventos $model */

$this->title = 'Update Modeloeventos: ' . $model->codigoevento;
$this->params['breadcrumbs'][] = ['label' => 'Modeloeventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoevento, 'url' => ['view', 'codigoevento' => $model->codigoevento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modeloeventos-update">

    <div style="border-left: 5px solid red; border-right: 5px solid red; padding-left: 10px; padding-right: 10px;">
        <h1 style="font-family: 'Times New Roman', Times, serif; font-weight: bold; font-size: 70px; margin-top: 90px; text-align: center;">ACTUALIZAR EVENTO</h1>
    </div>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
