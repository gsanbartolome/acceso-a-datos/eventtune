<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\modeloeventos $model */

$this->title = $model->codigoevento;
$this->params['breadcrumbs'][] = ['label' => 'Modeloeventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$botonesActivos = false; // Suponiendo que los botones de actualización y eliminación deben estar activos

\yii\web\YiiAsset::register($this);
?>

<div style="text-align: center; margin-top: 80px;">
    <div style="display: inline-block; position: relative;">
        <div style="position: absolute; top: 50%; transform: translateY(-50%); height: 100%; width: 3px; background-color: red; left: -10px;"></div>
        <h1 style="text-transform: uppercase; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 55px; margin: 0 20px;">
            <?= Html::encode($model->nombre) ?>
        </h1>
        <div style="position: absolute; top: 50%; transform: translateY(-50%); height: 100%; width: 3px; background-color: red; right: -10px;"></div>
    </div>
</div>

<div style="position: relative; left: -110px; top: -50px; min-height: 100vh; padding-bottom: 100px;">
    <div style="position: absolute; left: 210px; top: 60px;">
        
        <div style="position: absolute; width: 600px; min-height: 400px; left: 30px; top: 60px;">
            <?php
            if ($model->imagen) {
                echo Html::img(Yii::getAlias('@web/uploads/') . $model->imagen, ['width' => '100%', 'id' => 'event-image', 'style' => 'border: 5px solid red;']);
            } else {
                echo Html::img(Yii::getAlias('/eventtune/yii2-app-basic/assets/imagenes/eventune_stock.png'), ['width' => '100%', 'id' => 'event-image', 'style' => 'border: 5px solid red;']);
            }
            ?>
        </div>
    </div>

    <div class="modeloeventos-view" style="display: flex; flex-direction: column; align-items: flex-end; padding-left: 880px; padding-right: 25px;">
        <div style="position: absolute; top: 125px; left: 875px;">
            <div class="detail-field" style="padding: 10px; width: 700px;">
                <span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Evento:</span>
<span style="font-size: 20px; text-transform: uppercase;"><?= Html::encode($model->nombre) ?></span>
            </div>
        </div>
        

        <hr style="position: absolute; border-top: 2px solid black; width: 400px; margin: 180px; left:700px;">
        <hr style="position: absolute; border-top: 2px solid black; width: 550px; margin: 673px; left: 207px;">
        <hr style="position: absolute; border-left: 4px solid red; height: 480px; margin: 185px 0 0; right: 10px;">

<div class="detail-field" style="position: relative; left: -10px; padding: 10px; width: 540px; min-height: 475px; margin-top: 190px;">
    <div style="font-size: 20px; font-family: 'Times New Roman', Times, serif; text-align: justify; overflow-y: auto; max-height: 455px;">
        <?= Html::encode($model->informacion) ?>
    </div>
</div>

    </div>


</div>

    <div style="position: absolute; top: 370px; left: 280px;">
        <div class="form-group" style="position: absolute; top: 320px; left: 890px;">
        <?= $botonesActivos ? Html::a('Update', ['update', 'id' => $model->codigoevento], ['class' => 'btn btn-primary btn-lg', 'style' => 'width: 200px;']) : '' ?>
        </div>

        <div class="form-group" style="position: absolute; top: 320px; left: 650px;">
            <?= $botonesActivos ? Html::a('Delete', ['BORRAR', 'codigoevento' => $model->codigoevento], [
                'class' => 'btn btn-danger btn-lg',
                'style' => 'width: 200px;',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) : '' ?>
        </div>
    </div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var eventImage = document.getElementById('event-image');
        eventImage.addEventListener('click', function() {
            showImage(this.src);
        });

        function showImage(imageUrl) {
            document.body.style.overflow = 'hidden';
            var overlay = document.createElement("div");
            overlay.id = "image-overlay";
            overlay.style.position = "fixed";
            overlay.style.top = "0";
            overlay.style.left = "0";
            overlay.style.width = "100%";
            overlay.style.height = "100%";
            overlay.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
            overlay.style.zIndex = "1000";

            var img = document.createElement("img");
            img.src = imageUrl;
            img.style.position = "absolute";
            img.style.top = "50%";
            img.style.left = "50%";
            img.style.transform = "translate(-50%, -50%) scale(0.5)";
            img.style.maxWidth = "70%";
            img.style.maxHeight = "70%";
            img.style.transition = "transform 0.5s ease";
            img.style.zIndex = "1001";

            overlay.appendChild(img);
            document.body.appendChild(overlay);
            img.getBoundingClientRect();
            img.style.transform = "translate(-50%, -50%) scale(1)";
            
            overlay.onclick = function() {
                document.body.style.overflow = '';
                overlay.remove();
            };
        }

        var valoracionesContainer = document.getElementById('valoraciones-container');
        var imageHeight = eventImage.clientHeight;

        if (imageHeight > 400) {
            valoracionesContainer.style.top = (imageHeight + 100) + 'px';
        }
    });
</script>
