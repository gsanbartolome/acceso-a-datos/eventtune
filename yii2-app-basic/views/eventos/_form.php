<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/** @var yii\web\View $this */
/** @var app\models\modeloeventos $model */
/** @var yii\widgets\ActiveForm $form */

?>

<style>
    body {
        overflow-x: hidden;
        margin: 0;
        padding: 0;
        background: url('/eventtune/yii2-app-basic/assets/imagenes/banner1.png') no-repeat center center fixed;
        background-size: cover;
    }
.blue-blurred-bg {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: linear-gradient(to bottom, rgba(0, 0, 255, 0.8) 0%, rgba(0, 0, 255, 0.4) 100%);
    z-index: -3;
}

</style>
<div class="blue-blurred-bg"></div>

<br><br>

<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); z-index: -3;"></div>
<div style="position: absolute; top: 0; left: 50%; transform: translateX(-50%); width: 85%; height: 200%; background-color: white; z-index: -1; box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);"></div>

<div style="position: relative; margin-left: 60px; z-index: 2; padding: 20px;">
    <?php $form = ActiveForm::begin(['options' => ['style' => 'overflow: auto;']]); ?>

    <div style="position: absolute; width: 600px; height: 400px; left: -30px; top: 50px;">
        <?= $form->field($model, 'file', ['labelOptions' => ['style' => 'display:none;']])->widget(FileInput::className(), ['options' => ['accept' => 'file/*']]) ?>
    </div>

    <div style="display: flex; flex-direction: column; align-items: flex-end; margin-left: 450px; padding-right: 650px;">
        <div style="margin-right: -600px;">
            <?= $form->field($model, 'nombre')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Nombre</span>')->textInput(['maxlength' => true, 'style' => 'width: 450px; font-size: 16px;', 'placeholder' => 'Escribe el nombre de tu evento...']) ?>

            <?= $form->field($model, 'informacion')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Información</span>', ['class' => 'field-label'])->textarea(['rows' => 10, 'style' => 'width: calc(100% - 0px); font-size: 16px; resize: none;', 'placeholder' => 'Escribe la información del evento aquí...']) ?>
        </div>
    </div>
        <?= $form->field($model, 'codigoevento')->hiddenInput()->label(false) ?>


    <div class="form-group" style="position: absolute; top: 480px; left: 870px;">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn-lg', 'id' => 'submit-btn', 'style' => 'width: 200px;']) ?>
    </div>

    <div class="form-group" style="position: absolute; top: 480px; left: 620px;">
        <?= Html::resetButton('Limpiar Perfil', ['class' => 'btn btn-warning btn-lg', 'id' => 'reset-btn', 'style' => 'width: 200px;']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<br><br><br><br>
<br><br>

<?php
$this->registerJs("
    $('#submit-btn').click(function(event) {
        var preciosValue = $('#precios-input').val();
        if (preciosValue.startsWith('-')) {
            event.preventDefault();
            $('#precios-error').text('No aceptamos valores negativos');
        }
    });

    $('#reset-btn').click(function(event) {
        event.preventDefault();
        $('#modeloeventos-nombre').val('');
        $('#modeloeventos-informacion').val('');
        $('#modeloeventos-contactos').val('');
        $('#precios-input').val('');
        $('#modeloeventos-codigousuario').val('');
        $('#precios-error').text('');
    });
");
?>
