<?php

use app\models\modeloeventos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Modeloeventos';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('
    .main-container {
        display: flex;
        max-width: 2000px;
        margin-top: -10px;
        flex-wrap: wrap;
    }
    
    .eventos-container {
        flex: 1;
        display: flex;
        flex-wrap: wrap;
        width: 100%;
        margin-left: 25px;
    }

    .banda-item {
        background-color: #ffffff;
        border: 1px solid #cccccc;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1), 0px 0px 10px rgba(0, 0, 0, 0.1);
        padding: 20px;
        margin: 30px;
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 430px;
        height: 640px;
        text-align: center;
        position: relative;
        border: 2px solid black;
        cursor: pointer;
        transition: transform 0.3s ease; /* Añadir transición para el efecto */
    }

    .banda-item:hover {
        transform: scale(1.05); /* Efecto de expansión al pasar el ratón */
    }

    .banda-item img {
        height: 350px;
        width: 100%;
        object-fit: cover;
        margin-bottom: -120px;
        border: 4px solid red;
        cursor: default;
    }

    .banda-item-content {
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .banda-item-name {
        font-weight: bold;
        margin-top: -40px;
        font-size: 2em;
        margin-bottom: 10px;
        background-color: white;
        width: 100%;
        padding: 10px;
        box-sizing: border-box;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 100%;
        display: block;
        font-size: calc(1.5em + (1em - 1vw));
        position:absolute;
        text-transform: uppercase; /* Agregar esta línea para convertir el texto en mayúsculas */
    }

    .banda-item-profile {
        font-family: "Times New Roman", Times, serif;
        position:absolute;
        top: 69%;
        font-size: 1.2em;
        margin: 0 50px;
        text-align: justify;
    }
    
    .search-bar {
        text-align: center;
        margin-bottom: 4px;
    }
    
    .search-container {
        max-width: 2000px;
        margin: 0 auto;
        padding: 20px;
        margin-left: 4%;
        margin-bottom: -20px;
    }

    .consulta-container:hover .consulta-item {
        transform: scale(1.02);
    }
    
    .keyword-tag {
        background-color: red;
        color: white;
        border-radius: 5px;
        padding: 5px 10px;
        margin: 5px;
        transition: transform 0.3s ease;
    }

    .keyword-tag:hover {
        transform: scale(1.05);
    }
  ');

?>

<div style="position: relative; left: 27%; top: 95px; text-align: center; margin-left: 38%; margin-right: 38%;">
    <h1 style="font-family: 'Times New Roman', Times, serif;">CREA TU EVENTO</h1>
    <p>Aqui puedes crear el perfil de tu evento para que 
        todos aquellos interesados en ayudarte a llevarlo 
        a cabo con sus habilidades musicales puedan verlo
        ¡RECUERDA DEJAR TU INFORMACION DE CONTACTO!
        De esa forma todos podran contactar contigo en caso 
        de estar interesados.
    </p>
    <div class="create-event-button">
        <?= Html::a('CREAR PERFIL', ['create'], ['class' => 'btn btn-primary btn-crear-eventos']) ?>
    </div>
</div>

<div class="modeloeventos-index">
    <div style="position: relative; top: -160px; left: 15%; margin-right: 40%; margin-bottom: -120px;">
        <h1 style="font-size: 120px; font-family: 'Times New Roman', Times, serif; margin: 0; color: #333; text-shadow: 4px 4px 10px rgba(0, 0, 0, 0.5); z-index: 1;">
            EVENTOS
        </h1>
    </div>
    
<div class="search-container">
    <div class="search-bar" style="margin-bottom: 20px;">
        <?= Html::beginForm(['index'], 'get', ['class' => 'form-inline', 'id' => 'keyword-search-form']); ?>
        <?= Html::textInput('keyword', Yii::$app->request->get('keyword'), ['class' => 'form-control', 'placeholder' => 'Buscar por palabra clave', 'style' => 'width: 50%;', 'id' => 'keyword-input']) ?>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px;']) ?>
        <?= Html::endForm(); ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label>Palabras Clave:</label><br>
        <?= Html::a('COMUNIONES', ['index', 'keyword' => 'COMUNIONES'], ['class' => 'btn btn-default keyword-tag']) ?>
        <?= Html::a('BODA', ['index', 'keyword' => 'BODA'], ['class' => 'btn btn-default keyword-tag']) ?>
        <?= Html::a('CUMPLEAÑOS', ['index', 'keyword' => 'CUMPLEAÑOS'], ['class' => 'btn btn-default keyword-tag']) ?>
        <?= Html::a('BAR', ['index', 'keyword' => 'BAR'], ['class' => 'btn btn-default keyword-tag']) ?>
        <?= Html::a('FIESTA', ['index', 'keyword' => 'FIESTA'], ['class' => 'btn btn-default keyword-tag']) ?>
        <?= Html::a('CONVENCION', ['index', 'keyword' => 'CONVENCION'], ['class' => 'btn btn-default keyword-tag']) ?>
        </div>
    </div>
    
    <?php
    if (Yii::$app->request->get('keyword')) {
        if ($dataProvider->totalCount > 0) {
            echo "<div class='search-results-count' style='margin-top: 30px; margin-left: 10px; position: absolute;'><p style='font-weight: bold; font-size: 16px;'>Se encontraron " . $dataProvider->totalCount . " resultados.</p></div>";
        } else {
            echo "<div class='search-results-count' style='margin-top: 30px; margin-left: 10px; position: absolute;'><p style='font-weight: bold; font-size: 16px;'>No se encontraron resultados.</p></div>";
        }
    }
    ?>
</div>

    <br>
    <br>
    <br>
    <hr class="divider-line">
    <br>

    <div class="main-container">
        <div class="eventos-container">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => function ($model, $key, $index, $widget) {
                    $imagenPath = $model->imagen ? Yii::getAlias('@web/uploads/') . $model->imagen : Yii::getAlias('/eventtune/yii2-app-basic/assets/imagenes/eventune_stock.png');
                    return '
                    <div class="consulta-container">
                        <div class="vertical-line"></div>
                        <div class="central-container">
                            <div class="banda-item consulta-item" onclick="window.location.href=\'' . Url::to(['view', 'codigoevento' => $model->codigoevento]) . '\'">
                                <img src="' . $imagenPath . '" onclick="event.stopPropagation(); showImage(\'' . $imagenPath . '\')">
                                <div class="banda-item-content">
                                    <div class="banda-item-name">' . Html::encode($model->nombre) . '</div>
                                    <div class="banda-item-profile">' . Html::encode(substr($model->informacion, 0, 210) . '...') . '</div>
                                </div>
                            </div>
                        </div>
                    </div>';
                },
                'options' => [
                    'tag' => false,
                ],
                'itemOptions' => [
                    'tag' => false,
                ],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'options' => ['class' => 'pagination', 'style' => 'justify-content: flex-end;'],
                    'linkOptions' => ['class' => 'page-link'],
                    'activePageCssClass' => 'active-page',
                ],
            ]); ?>
        </div>
    </div>

    <br>
    <br>
    <br>
</div>

<script>
    function showImage(imageUrl) {
        document.body.style.overflow = 'hidden';

        var overlay = document.createElement("div");
        overlay.id = "image-overlay";
        overlay.style.position = "fixed";
        overlay.style.top = "0";
        overlay.style.left = "0";
        overlay.style.width = "100%";
        overlay.style.height = "100%";
        overlay.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
        overlay.style.zIndex = "1000";

        var img = document.createElement("img");
        img.src = imageUrl;
        img.style.position = "absolute";
        img.style.top = "50%";
        img.style.left = "50%";
        img.style.transform = "translate(-50%, -50%) scale(0.5)";
        img.style.maxWidth = "70%";
        img.style.maxHeight = "70%";
        img.style.transition = "transform 0.5s ease";
        img.style.zIndex = "1001";

        overlay.appendChild(img);
        document.body.appendChild(overlay);
        img.getBoundingClientRect();
        img.style.transform = "translate(-50%, -50%) scale(1)";

        overlay.onclick = function() {
            document.body.style.overflow = '';
            overlay.remove();
        };
        }
        
    document.addEventListener('DOMContentLoaded', function() {
        const keywordTags = document.querySelectorAll('.keyword-tag');

        keywordTags.forEach(tag => {
            tag.addEventListener('click', function(event) {
                const keyword = event.target.textContent;
                document.getElementById('keyword-input').value = keyword;
            });
        });
    });
        </script>
