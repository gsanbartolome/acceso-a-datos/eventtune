<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\modelosolistas $model */

$this->title = 'Create Modelosolistas';
$this->params['breadcrumbs'][] = ['label' => 'Modelosolistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modelosolistas-create">

    <div style="border-left: 5px solid red; border-right: 5px solid red; padding-left: 10px; padding-right: 10px;">
        <h1 style="font-family: 'Times New Roman', Times, serif; font-weight: bold; font-size: 70px; margin-top: 90px; text-align: center;">PERFIL SOLISTAS</h1>
    </div>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
