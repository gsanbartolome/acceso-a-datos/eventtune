<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/** @var yii\web\View $this */
/** @var app\models\modelosolistas $model */
/** @var yii\widgets\ActiveForm $form */

?>

<style>
    body {
        overflow-x: hidden;
        margin: 0;
        padding: 0;
        background: url('/eventtune/yii2-app-basic/assets/imagenes/banner1.png') no-repeat center center fixed;
        background-size: cover;
    }
    .red-blurred-bg {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%);
        z-index: -3;
    }
</style>
<div class="red-blurred-bg"></div>

<br><br>

<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); z-index: -3;"></div>

<div style="position: absolute; top: 0; left: 50%; transform: translateX(-50%); width: 85%; height: 200%; background-color: white; z-index: -1; box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);"></div>

<div style="position: relative; margin-left: 60px; z-index: 2; padding: 20px;">
    <?php $form = ActiveForm::begin(['options' => ['style' => 'overflow: auto;']]); ?>

    
    <!-- Campo de archivo -->
    <div style="position: absolute; width: 600px; height: 400px; left: -30px; top: 50px;">
        <?= $form->field($model, 'file', ['labelOptions' => ['style' => 'display:none;']])->widget(FileInput::className(), ['options' => ['accept' => 'file/*']]) ?>
    </div>
    <!-- Formulario principal -->
    <div style="display: flex; flex-direction: column; align-items: flex-end; margin-left: 450px; padding-right: 650px;">
        <!-- Campos de texto -->
        <div style="margin-right: -600px;">
            <?= $form->field($model, 'nombre')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Nombre</span>')
                ->textInput(['maxlength' => true, 'style' => 'width: 450px; font-size: 16px;', 'placeholder' => 'Escribe el nombre del solista...']) ?>

            <?= $form->field($model, 'informaciondeperfil')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Información de Perfil</span>')
                ->textarea(['rows' => 10, 'style' => 'width: calc(100% - 0px); font-size: 16px; resize: none;', 'placeholder' => 'Escribe la información de perfil aquí...']) ?>

            <?= $form->field($model, 'contactos')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Contactos</span>')
                ->textarea(['rows' => 5, 'style' => 'width: calc(100% - 0px); font-size: 16px; resize: none;', 'placeholder' => 'Escribe los contactos aquí...']) ?>
        </div>
    </div>

    <!-- Campo de precios -->
    <div style="position: absolute; top: 443px; left: -30px;">
        <?= $form->field($model, 'precio')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Precio</span>')
            ->textInput(['maxlength' => true, 'id' => 'precio-input', 'style' => 'width: 200px; font-size: 16px;', 'placeholder' => 'Escribe el precio aquí...']) ?>
        <div id="precio-error" style="color: red; margin-top: 5px;"></div>
    </div>


    <!-- Botón de guardar -->
    <div class="form-group" style="position: absolute; top: 480px; left: 400px;">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn-lg', 'id' => 'submit-btn', 'style' => 'width: 200px;']) ?>
    </div>

    <!-- Botón de limpiar perfil -->
    <div class="form-group" style="position: absolute; top: 480px; left: 190px;">
        <?= Html::resetButton('Limpiar Perfil', ['class' => 'btn btn-warning btn-lg', 'id' => 'reset-btn', 'style' => 'width: 200px;']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<br><br>

<?php
$this->registerJs("
    $('#submit-btn').click(function(event) {
        var precioValue = $('#precio-input').val();
        if (precioValue.startsWith('-')) {
            event.preventDefault();
            $('#precio-error').text('No aceptamos valores negativos');
        }
    });

    $('#reset-btn').click(function(event) {
        event.preventDefault();
        $('#modelosolistas-nombre').val('');
        $('#modelosolistas-informaciondeperfil').val('');
        $('#modelosolistas-contactos').val('');
        $('#precio-input').val('');
        $('#modelosolistas-valoraciones').val('');
        $('#precio-error').text('');
    });
");
?>
