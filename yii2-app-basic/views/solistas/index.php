<?php

use app\models\modelosolistas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var app\models\solistasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Modelosolistas';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('
    .main-container {
        display: flex;
        justify-content: center;
        max-width: 2000px;
        margin-top: -10px;
    }

    .central-container {
        width: 39%;
        margin: 0 auto;
    }

    .solista-item {
        background-color: #ffffff;
        border: 1px solid #cccccc;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1), 0px 0px 10px rgba(0, 0, 0, 0.1);
        padding: 20px;
        margin: 30px;
        display: flex;
        align-items: center;
        width: 900px;
        height: 310px;
        text-align: left;
        position: relative;
        border: 2px solid black;
            cursor: pointer; /* Añadir cursor pointer para indicar que es clicable */

    }

    .solista-item img {
        height: 260px;
        width: 350px;
        margin-right: 20px;
        align-self: center;
        border: 4px solid red;
            cursor: default; /* No mostrar el cursor pointer sobre la imagen */

    }

    .solista-item-content {
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }

    .solista-item-name {
        font-weight: bold;
        font-size: 2.4em;
        margin-bottom: 10px;
        position: absolute;
        left: 43%;
        top: 10px;
        background-color: white;
        width: calc(60% - 40px);
        padding: 10px;
        box-sizing: border-box;
        z-index: 2;
    }

    .solista-item-actions {
        position: absolute;
        bottom: 0;
        right: 0;
        margin-bottom: 10px;
        margin-right: 10px;
    }

    .solista-item-profile {
        font-family: "Times New Roman", Times, serif;
        font-size: 1.2em;
        position: relative;
        margin-left: 9px;
        margin-right: 40px;
        margin-top: 0px;
        text-align: justify;
    }

    .solista-item-price {
        font-size: 1.2em;
        position: absolute;
        left: 43.5%;
        top: 250px;
        margin-left: 10px;
        margin-right: 20px;
        text-align: justify;
        font-weight: bold;
    }

    .search-bar {
        text-align: center;
        margin-bottom: 4px;
    }
    
    .search-container {
        max-width: 2000px;
        margin: 0 auto;
        padding: 20px;
        margin-left: 4%;
        margin-bottom: -20px;
    }

    .divider-line {
        border-top: 0.5px solid black;
        margin: 20px auto;
        width: 90%;
    }
    
    .modelosolistas-index {
        width: 1510px;
        margin: 0 auto;
    }

    .btn-crear-solistas {
        margin-right: 10px;
        background-color: blue;
        color: white;
    }
    
    .animated-item {
        animation: dropdownAnimation 0.5s ease forwards;
        opacity: 0;
        transform: translateY(-20px);
    }

    @keyframes dropdownAnimation {
        from {
            opacity: 0;
            transform: translateY(-20px);
        }
        to {
            opacity: 1;
            transform: translateY(0);
        }
    }

    .consulta-item {
        transition: transform 0.3s ease;
    }

    .consulta-item:hover {
        transform: translateX(-20px);
    }

    .consulta-container {
        display: flex;
        position: relative;
    }

    .vertical-line {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 31.2%;
        width: 3px;
        background-color: red;
        z-index: 4;
    }

    .central-container {
        z-index: 2;
    }
    
    .pagination .active-page a {
        text-decoration: underline !important;
        font-weight: bold !important; /* Esto es opcional, por si quieres resaltar más */
    }
');

?>

<div style="position: relative; left: 27%; top: 95px; text-align: center; margin-left: 38%; margin-right: 38%;">
<h1 style="font-family: 'Times New Roman', Times, serif;">CREA TU PERFIL DE SOLISTA</h1>
<p>¡Embárcate en el emocionante viaje de la creación 
    de tu propio perfil como solista musical! Sumérgete en la experiencia 
    única de construir un perfil que capture la esencia y 
    la energía de tu talento artístico. 
    ¡Descubre un mundo lleno de posibilidades musicales! 
</p>

    <div class="create-band-button">
        <?= Html::a('CREAR PERFIL', ['create'], ['class' => 'btn btn-primary btn-crear-solistas']) ?>
    </div>
</div>


<div class="modelosolistas-index">
 
    
    <div style="position: relative; top: -190px; left: 15%; margin-right: 70%; margin-bottom: -120px;">
        <h1 style="font-size: 120px; font-family: 'Times New Roman', Times, serif; margin: 0; color: #333; text-shadow: 4px 4px 10px rgba(0, 0, 0, 0.5); z-index: 1;">
            SOLISTAS
        </h1>
    </div>

  

    <div class="search-container">
        <div class="search-bar" style="margin-bottom: 20px;">
            <?= Html::beginForm(['index'], 'get', ['class' => 'form-inline', 'id' => 'keyword-search-form']); ?>
            <?= Html::textInput('keyword', Yii::$app->request->get('keyword'), ['class' => 'form-control', 'placeholder' => 'Buscar por palabra clave', 'style' => 'width: 50%;', 'id' => 'keyword-input']) ?>
            <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px;']) ?>
            <?= Html::endForm(); ?>
        </div>
            
        <div class="row">
            <div class="col-md-6">
                <?= Html::beginForm(['index'], 'get', ['class' => 'form-inline', 'id' => 'price-search-form']); ?>
                <?= Html::input('number', 'minPrice', Yii::$app->request->get('minPrice'), ['class' => 'form-control', 'placeholder' => 'Precio mínimo', 'min' => '0', 'id' => 'minPrice']); ?>
                <?= Html::input('number', 'maxPrice', Yii::$app->request->get('maxPrice'), ['class' => 'form-control', 'placeholder' => 'Precio máximo', 'min' => '0', 'id' => 'maxPrice']); ?>
                <?= Html::submitButton('Filtrar por precio', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px;']) ?>
                <?= Html::endForm(); ?>
            </div>
        </div>
            
        <?php
        if (Yii::$app->request->get('keyword')) {
            if ($dataProvider->totalCount > 0) {
                echo "<div class='search-results-count' style='margin-top: 30px; margin-left: 10px; position: absolute;'><p style='font-weight: bold; font-size: 16px;'>Se encontraron " . $dataProvider->totalCount . " resultados.</p></div>";
            } else {
                echo "<div class='search-results-count' style='margin-top: 30px; margin-left: 10px; position: absolute;'><p style='font-weight: bold; font-size: 16px;'>No se encontraron resultados.</p></div>";
            }
        }
        ?>
    </div>
    
    <br>
    <br>

  
    <hr class="divider-line">
    <br>

    <div class="main-container">
        <div style="display: flex; margin-left: -70px; margin-top: 40px;">
            <h2 style="font-weight: bold; font-size: 43px; font-family: 'Times New Roman', Times, serif;">TAGS</h2>
        </div>

        <!-- Desplegable de MUSICA -->
        <div style="display: flex; justify-content: flex-start; margin-top: 300px; margin-left: -290px;">
            <div class="dropdown">
                <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none; border: none; font-weight: bold; font-size: 24px; border-left: 3px solid red;">
                    MUSICA
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="padding: 0; border: none; margin-left: 110px;">
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('ROCK')" style="font-style: italic; font-size: 20px;">
                        <span style="color: red;">-</span> ROCK
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('POP')" style="font-style: italic; font-size: 20px;">
                        <span style="color: red;">-</span> POP
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('EXPERIMENTAL')" style="font-style: italic; font-size: 20px;">
                        <span style="color: red;">-</span> EXPERIMENTAL
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('REMIX')" style="font-style: italic; font-size: 20px;">
                        <span style="color: red;">-</span> REMIX
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('COVERS')" style="font-style: italic; font-size: 20px;">
                        <span style="color: red;">-</span> COVERS
                    </a>
                </div>
            </div>
        </div>

        <!-- Nuevo Desplegable -->
        <div style="display: flex; justify-content: flex-start; margin-top: 600px; margin-left: -120px;">
            <div class="dropdown">
                <button class="btn" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none; border: none; font-weight: bold; font-size: 24px; border-left: 3px solid blue;">
                    INSTRUMENTOS
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2" style="padding: 0; border: none; margin-left: 110px;">
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('GUITARRA')" style="font-style: italic; font-size: 20px;">
                        <span style="color: blue;">-</span> GUITARRA
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('PIANO')" style="font-style: italic; font-size: 20px;">
                        <span style="color: blue;">-</span> PIANO
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('BATERIA')" style="font-style: italic; font-size: 20px;">
                        <span style="color: blue;">-</span> BATERIA
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('DJ')" style="font-style: italic; font-size: 20px;">
                        <span style="color: blue;">-</span> DJ
                    </a>
                    <a class="dropdown-item animated-item" href="#" onclick="addKeyword('BAJO')" style="font-style: italic; font-size: 20px;">
                        <span style="color: blue;">-</span> BAJO
                    </a>
                </div>
            </div>
        </div>
            <!-- Desplegable de GÉNEROS -->
    <div style="display: flex; justify-content: flex-start; margin-top: 900px; margin-left: -210px;">
        <div class="dropdown">
            <button class="btn" type="button" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none; border: none; font-weight: bold; font-size: 24px; border-left: 3px solid green;">
                GÉNEROS
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3" style="padding: 0; border: none; margin-left: 110px;">
                <a class="dropdown-item animated-item" href="#" onclick="addKeyword('JAZZ')" style="font-style: italic; font-size: 20px;">
                    <span style="color: green;">-</span> JAZZ
                </a>
                <a class="dropdown-item animated-item" href="#" onclick="addKeyword('BLUES')" style="font-style: italic; font-size: 20px;">
                    <span style="color: green;">-</span> BLUES
                </a>
                <a class="dropdown-item animated-item" href="#" onclick="addKeyword('FUNK')" style="font-style: italic; font-size: 20px;">
                    <span style="color: green;">-</span> FUNK
                </a>
                <a class="dropdown-item animated-item" href="#" onclick="addKeyword('CLASSIC')" style="font-style: italic; font-size: 20px;">
                    <span style="color: green;">-</span> CLASSIC
                </a>
                <a class="dropdown-item animated-item" href="#" onclick="addKeyword('HIP-HOP')" style="font-style: italic; font-size: 20px;">
                    <span style="color: green;">-</span> HIP-HOP
                </a>
            </div>
        </div>
    </div>


        
        <div class="solistas-container">
    <?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model, $key, $index, $widget) {
        $imagenPath = $model->imagen ? Yii::getAlias('@web/uploads/') . $model->imagen : Yii::getAlias('/eventtune/yii2-app-basic/assets/imagenes/eventune_stock.png');
        return '
        <div class="consulta-container">
            <div class="vertical-line"></div>
            <div class="central-container">
                <div class="solista-item consulta-item" onclick="window.location.href=\'' . Url::to(['view', 'codigosolista' => $model->codigosolista]) . '\'">
                    <img src="' . $imagenPath . '" onclick="event.stopPropagation(); showImage(\'' . $imagenPath . '\')">
                    <div class="solista-item-content">
                        <div class="solista-item-name">' . Html::encode($model->nombre) . '</div>
                        <div class="solista-item-profile">' . Html::encode(substr($model->informaciondeperfil, 0, 210) . '...') . '</div>
                        <div class="solista-item-price">' . Html::encode($model->precio . '€') . '</div>
                        
                    </div>
                </div>
            </div>
        </div>';
    },
    'options' => [
        'tag' => false,
    ],
    'itemOptions' => [
        'tag' => false,
    ],
    'layout' => "{items}\n{pager}",
    'pager' => [
        'options' => ['class' => 'pagination', 'style' => 'justify-content: flex-end;'], // Aquí aplicas el estilo
        'linkOptions' => ['class' => 'page-link'],
        'activePageCssClass' => 'active-page', // Clase CSS para la página activa
    ],
]); ?>

        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</div>

<script>
    document.getElementById('price-search-form').onsubmit = function() {
        var minPrice = document.getElementById('minPrice').value;
        var maxPrice = document.getElementById('maxPrice').value;
        if (parseInt(minPrice) > parseInt(maxPrice)) {
            alert('El precio mínimo no puede ser mayor que el precio máximo.');
            return false;
        }
    };
    
    function showImage(imageUrl) {
        document.body.style.overflow = 'hidden';

        var overlay = document.createElement("div");
        overlay.id = "image-overlay";
        overlay.style.position = "fixed";
        overlay.style.top = "0";
        overlay.style.left = "0";
        overlay.style.width = "100%";
        overlay.style.height = "100%";
        overlay.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
        overlay.style.zIndex = "1000";

        var img = document.createElement("img");
        img.src = imageUrl;
        img.style.position = "absolute";
        img.style.top = "50%";
        img.style.left = "50%";
        img.style.transform = "translate(-50%, -50%) scale(0.5)";
        img.style.maxWidth = "70%";
        img.style.maxHeight = "70%";
        img.style.transition = "transform 0.5s ease";
        img.style.zIndex = "1001";

        overlay.appendChild(img);
        document.body.appendChild(overlay);
        img.getBoundingClientRect();
        img.style.transform = "translate(-50%, -50%) scale(1)";
        
        overlay.onclick = function() {
            document.body.style.overflow = '';
            overlay.remove();
        };
    }

    function addKeyword(keyword) {
        var input = document.getElementById('keyword-input');
        var currentValue = input.value;
        if (currentValue.includes(keyword)) {
            return;
        }
        input.value = currentValue ? currentValue + ' ' + keyword : keyword;
    }
    
    document.getElementById('dropdownMenuButton').addEventListener('click', function() {
        var items = document.querySelectorAll('.animated-item');
        items.forEach(function(item, index) {
            setTimeout(function() {
                item.style.animationDelay = (index * 0.1) + 's';
                item.classList.toggle('visible');
            }, index * 100);
        });
    });

    document.getElementById('dropdownMenuButton2').addEventListener('click', function() {
        var items = document.querySelectorAll('.animated-item');
        items.forEach(function(item, index) {
            setTimeout(function() {
                item.style.animationDelay = (index * 0.1) + 's';
                item.classList.toggle('visible');
            }, index * 100);
        });
    });

    document.addEventListener('DOMContentLoaded', function() {
        var consultaItems = document.querySelectorAll('.consulta-item');

        consultaItems.forEach(function(item) {
            item.addEventListener('mouseleave', function() {
                this.style.transform = 'translateX(0)';
            });

            item.addEventListener('transitionend', function() {
                // Restablecer la transformación después de que la transición haya finalizado
                this.style.transform = '';
            });
        });
    });
</script>
