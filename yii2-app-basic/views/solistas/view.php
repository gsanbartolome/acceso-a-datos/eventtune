<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\file\FileInput;

/** @var yii\web\View $this */
/** @var app\models\modelosolistas $model */

$this->title = $model->codigosolista;
$this->params['breadcrumbs'][] = ['label' => 'Modelosolistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$botonesActivos = false;

\yii\web\YiiAsset::register($this);
?>

<div style="text-align: center; margin-top: 80px;">
    <div style="display: inline-block; position: relative;">
        <div style="position: absolute; top: 50%; transform: translateY(-50%); height: 100%; width: 3px; background-color: red; left: -10px;"></div>
        <h1 style="text-transform: uppercase; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 55px; margin: 0 20px;">
            <?= Html::encode($model->nombre) ?>
        </h1>
        <div style="position: absolute; top: 50%; transform: translateY(-50%); height: 100%; width: 3px; background-color: red; right: -10px;"></div>
    </div>
</div>

<div style="position: relative; left: -110px; top: -50px; min-height: 100vh; padding-bottom: 100px;">
    <div style="position: absolute; left: 210px; top: 60px;">
        
        <div style="position: absolute; width: 600px; min-height: 400px; left: 30px; top: 60px;">
            <?php
            if ($model->imagen) {
                echo Html::img(Yii::getAlias('@web/uploads/') . $model->imagen, ['width' => '100%', 'id' => 'solista-image', 'style' => 'border: 5px solid red;']);
            } else {
                echo Html::img(Yii::getAlias('/eventtune/yii2-app-basic/assets/imagenes/eventune_stock.png'), ['width' => '100%', 'id' => 'solista-image', 'style' => 'border: 5px solid red;']);
            }
            ?>
        </div>

        <div id="valoraciones-container" style="position: absolute; top: 480px; left: 30px;">
            <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['style' => 'overflow: auto;']]); ?>
            <?= $form->field($model, 'valoraciones')->textarea([
                'rows' => 5,
                'style' => 'width: 600px; font-size: 16px; resize: none;',
                'placeholder' => 'Añade aquí tu valoración...',
                'value' => ''
            ])->label(false) ?>
            <div class="form-group">
                <?= Html::submitButton('Subir Valoración', ['class' => 'btn btn-success', 'style' => 'width: 200px; margin-top: -5px; margin-left: 400px;']) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>

    <div class="modelosolistas-view" style="display: flex; flex-direction: column; align-items: flex-end; padding-left: 880px; padding-right: 25px;">
        <div style="position: absolute; top: 125px; left: 875px;">
            <div class="detail-field" style="padding: 10px; width: 400px;">
                <span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Precios:</span>
                <span style="font-size: 20px;"><?= Html::encode($model->precio) ?></span>
                <span style="font-size: 20px;">€</span>
            </div>
        </div>
        
        <hr style="position: absolute; border-top: 2px solid black; width: 400px; margin: 120px; left:760px;">
        <hr style="position: absolute; border-top: 2px solid black; width: 400px; margin: 180px; left:700px;">
        <hr style="position: absolute; border-top: 2px solid black; width: 550px; margin: 673px; left: 207px;">
        <hr style="position: absolute; border-top: 2px solid black; width: 550px; margin: 758px; left: 122px;">
        <hr style="position: absolute; border-left: 4px solid red; height: 480px; margin: 185px 0 0; right: 10px;">

        <div class="detail-field" style="position: relative; left: -10px; padding: 10px; width: 540px; min-height: 475px; margin-top: 190px;">
            <div style="font-size: 20px; font-family: 'Times New Roman', Times, serif; text-align: justify; overflow-y: auto; max-height: 455px;">
                <?php 
                    $texto = Html::encode($model->informaciondeperfil);
                    echo ucfirst(mb_strtolower($texto)); ?>
                <br><br>
                <strong>Contactos:</strong>
                <br>
                <?php 
                    $contactos = Html::encode($model->contactos);
                    $contactos_con_guiones = '- ' . str_replace("\n", "\n- ", $contactos);
                    echo nl2br($contactos_con_guiones); ?>
            </div>
        </div>
    </div>

    <div style="position: absolute; top: 370px; left: 280px;">
        <div class="form-group" style="position: absolute; top: 320px; left: 650px;">
            <?= $botonesActivos ? Html::a('Update', ['update', 'codigosolista' => $model->codigosolista], ['class' => 'btn btn-primary btn-lg', 'style' => 'width: 200px;']) : '' ?>
        </div>

        <div class="form-group" style="position: absolute; top: 320px; left: 890px;">
            <?= $botonesActivos ? Html::a('Delete', ['delete', 'codigosolista' => $model->codigosolista], [
                'class' => 'btn btn-danger btn-lg',
                'style' => 'width: 200px;',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) : '' ?>
        </div>
    </div>
    
<div style="position: relative; margin-top: 30px; margin-left: 880px; width: 550px;">
    <h3 style="font-family: Times New Roman; font-weight: bold; text-align: center;">Valoraciones</h3><br><br><br>
    <div id="valoraciones-list" style="display: flex; flex-direction: column; gap: 20px;">
        <?php
        $valoraciones = explode("\n", $model->getOldAttribute('valoraciones'));
        $valoracionesCount = count($valoraciones);
        $displayedValoraciones = min(3, $valoracionesCount); // Mostrar solo 3 valoraciones inicialmente
        for ($i = 0; $i < $displayedValoraciones; $i++) {
            if (trim($valoraciones[$i]) !== '') {
                echo '<div style="background-color: #ff6666; color: white; padding: 15px; border-radius: 10px; width: 100%; box-sizing: border-box; min-height: 80px; font-weight: bold; transition: background-color 0.3s ease;" onmouseover="this.style.backgroundColor=\'#ff9999\'" onmouseout="this.style.backgroundColor=\'#ff6666\'">' . nl2br(Html::encode($valoraciones[$i])) . '</div>';
            }
        }
        ?>
        <?php if ($valoracionesCount > 3): ?>
                <div id="rest-valoraciones" style="display: none;">
                    <?php
                    for ($i = 3; $i < $valoracionesCount; $i++) {
                        if (trim($valoraciones[$i]) !== '') {
                            echo '<div style="background-color: #ff6666; color: white; padding: 15px; border-radius: 10px; width: 100%; box-sizing: border-box; min-height: 80px; font-weight: bold; margin-bottom: 22px; transition: background-color 0.3s ease;" onmouseover="this.style.backgroundColor=\'#ff9999\'" onmouseout="this.style.backgroundColor=\'#ff6666\'">' . nl2br(Html::encode($valoraciones[$i])) . '</div>';
                        }
                    }
                    ?>
                </div>
            <div id="show-more" style="color: #ff6666; cursor: pointer; margin-left: 18px;">Pulsar para ver más valoraciones</div>
        <?php endif; ?>
    </div>
</div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var solistaImage = document.getElementById('solista-image');
        solistaImage.addEventListener('click', function() {
            showImage(this.src);
        });

        function showImage(imageUrl) {
            document.body.style.overflow = 'hidden';
            var overlay = document.createElement("div");
            overlay.id = "image-overlay";
            overlay.style.position = "fixed";
            overlay.style.top = "0";
            overlay.style.left = "0";
            overlay.style.width = "100%";
            overlay.style.height = "100%";
            overlay.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
            overlay.style.zIndex = "1000";

            var img = document.createElement("img");
            img.src = imageUrl;
            img.style.position = "absolute";
            img.style.top = "50%";
            img.style.left = "50%";
            img.style.transform = "translate(-50%, -50%) scale(0.5)";
            img.style.maxWidth = "70%";
            img.style.maxHeight = "70%";
            img.style.transition = "transform 0.5s ease";
            img.style.zIndex = "1001";

            overlay.appendChild(img);
            document.body.appendChild(overlay);
            img.getBoundingClientRect();
            img.style.transform = "translate(-50%, -50%) scale(1)";
            
            overlay.onclick = function() {
                document.body.style.overflow = '';
                overlay.remove();
            };
        }

        var valoracionesContainer = document.getElementById('valoraciones-container');
        var imageHeight = solistaImage.clientHeight;

        if (imageHeight > 400) {
            valoracionesContainer.style.top = (imageHeight + 100) + 'px';
        }
    });
    
    document.addEventListener('DOMContentLoaded', function() {
        var showMoreButton = document.getElementById('show-more');
        var restValoraciones = document.getElementById('rest-valoraciones');

        if (showMoreButton) {
            showMoreButton.addEventListener('click', function() {
                restValoraciones.style.display = 'block';
                showMoreButton.style.display = 'none';
            });
        }
    });
</script>
