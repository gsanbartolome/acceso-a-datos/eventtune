<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/** @var yii\web\View $this */
/** @var app\models\modelobandas $model */
/** @var yii\widgets\ActiveForm $form */

?>

<style>
    body {
        overflow-x: hidden;
        margin: 0;
        padding: 0;
        background: url('/eventtune/yii2-app-basic/assets/imagenes/banner1.png') no-repeat center center fixed;
        background-size: cover;
    }
                .red-blurred-bg {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); /* Cambia los valores de los colores y la opacidad según tus preferencias */
            z-index: -3; /* Coloca el fondo detrás de otros elementos */
        }
</style>
<div class="red-blurred-bg"></div>

<br><br>

<!-- Contenedor principal con fondo rojo difuminado -->
<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: linear-gradient(to bottom, rgba(255, 0, 0, 0.5) 0%, rgba(255, 0, 0, 0.1) 100%); z-index: -3;"></div>

<!-- Cuadro blanco -->
<div style="position: absolute; top: 0; left: 50%; transform: translateX(-50%); width: 85%; height: 200%; background-color: white; z-index: -1; box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);"></div>

<div style="position: relative; margin-left: 60px; z-index: 2; padding: 20px;"> <!-- Contenedor principal con contenido -->
    <?php $form = ActiveForm::begin(['options' => ['style' => 'overflow: auto;']]); ?>
    


    <!-- Contenedor para el campo de archivo -->
    <div style="position: absolute; width: 600px; height: 400px; left: -30px; top: 50px;">
        <?= $form->field($model, 'file', ['labelOptions' => ['style' => 'display:none;']])->widget(FileInput::className(), ['options' => ['accept' => 'file/*']]) // Campo de archivo ?>
    </div>

    <!-- Formulario principal -->
    <div style="display: flex; flex-direction: column; align-items: flex-end; margin-left: 450px; padding-right: 650px;">
        <!-- Contenedor para los campos de texto -->
        <div style="margin-right: -600px;">
            <!-- Campo Nombre con marcador de posición -->
            <?= $form->field($model, 'nombre')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Nombre</span>')->textInput(['maxlength' => true, 'style' => 'width: 450px; font-size: 16px;', 'placeholder' => 'Escribe el nombre de tu banda...']) // Campo Nombre ?>

            <!-- Campo Información de Perfil con marcador de posición -->
            <?= $form->field($model, 'informaciondeperfil')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Información de Perfil</span>', ['class' => 'field-label'])->textarea(['rows' => 10, 'style' => 'width: calc(100% - 0px); font-size: 16px; resize: none;', 'placeholder' => 'Escribe tu información de perfil aquí...']) // Campo Información de Perfil ?>

            <!-- Campo Contactos con marcador de posición -->
            <div class="form-group field-modelobandas-contactos" style="margin-top: 30px;">
                <?= $form->field($model, 'contactos')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Contactos</span>', ['class' => 'field-label'])->textarea(['rows' => 5, 'style' => 'width: calc(100% - 0px); font-size: 16px; resize: none;', 'placeholder' => 'Escribe tus contactos aquí...']) // Campo Contactos ?>
            </div>
        </div>
    </div>

    <!-- Contenedor para el campo de precios -->
    <div style="position: absolute; top: 443px; left: -30px;">
        <?= $form->field($model, 'precios')->label('<span style="font-family: Times New Roman; font-weight: bold; font-size: 23px;">Precios</span>', ['class' => 'field-label'])->textInput(['maxlength' => true, 'id' => 'precios-input', 'style' => 'width: 200px; font-size: 16px;', 'placeholder' => 'Escribe tus precios aquí...']) // Campo Precios ?>
        <div id="precios-error" style="color: red; margin-top: 5px;"></div>
    </div>

    <!-- Contenedor para el campo de código de usuario -->

    <!-- Botón de enviar -->
    <div class="form-group" style="position: absolute; top: 480px; left: 400px;">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn-lg', 'id' => 'submit-btn', 'style' => 'width: 200px;']) // Botón de enviar ?>
    </div>

    <!-- Botón de limpiar perfil -->
    <div class="form-group" style="position: absolute; top: 480px; left: 190px;">
        <?= Html::resetButton('Limpiar Perfil', ['class' => 'btn btn-warning btn-lg', 'id' => 'reset-btn', 'style' => 'width: 200px;']) // Botón de limpiar perfil ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<br><br>

<?php
// Registro de script JavaScript para validación y reseteo de formulario
$this->registerJs("
    // Validación del campo de precios para no aceptar valores negativos
    $('#submit-btn').click(function(event) {
        var preciosValue = $('#precios-input').val();
        if (preciosValue.startsWith('-')) {
            event.preventDefault();
            $('#precios-error').text('No aceptamos valores negativos');
        }
    });

    // Reseteo del formulario y limpieza de campos
    $('#reset-btn').click(function(event) {
        event.preventDefault(); // Evitar el comportamiento predeterminado del botón de reset
        $('#modelobandas-nombre').val(''); // Limpiar el campo de nombre
        $('#modelobandas-informaciondeperfil').val(''); // Limpiar el campo de información de perfil
        $('#modelobandas-contactos').val(''); // Limpiar el campo de contactos
        $('#precios-input').val(''); // Limpiar el campo de precios
        $('#modelobandas-codigousuario').val(''); // Limpiar el campo de código de usuario
        $('#precios-error').text(''); // Limpiar cualquier mensaje de error
    });
");
?>
