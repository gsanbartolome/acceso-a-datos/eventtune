<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\bandasSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="modelobandas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codigobanda') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'precios') ?>

    <?= $form->field($model, 'informaciondeperfil') ?>

    <?= $form->field($model, 'valoraciones') ?>

    <?php // echo $form->field($model, 'codigousuario') ?>

    <!-- Agregar campos ocultos para el filtrado por palabra clave y rango de precios -->
    <?= Html::activeHiddenInput($model, 'keyword') ?>
    <?= Html::activeHiddenInput($model, 'minPrice') ?>
    <?= Html::activeHiddenInput($model, 'maxPrice') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
