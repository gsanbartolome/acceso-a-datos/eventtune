<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\modelobandas $model */

$this->title = 'Update Modelobandas: ' . $model->codigobanda;
$this->params['breadcrumbs'][] = ['label' => 'Modelobandas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigobanda, 'url' => ['view', 'codigobanda' => $model->codigobanda]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modelobandas-update">

    <div style="border-left: 5px solid red; border-right: 5px solid red; padding-left: 10px; padding-right: 10px;">
        <h1 style="font-family: 'Times New Roman', Times, serif; font-weight: bold; font-size: 70px; margin-top: 90px; text-align: center;">ACTUALIZAR PERFIL</h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
