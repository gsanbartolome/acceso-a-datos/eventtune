<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bandas".
 *
 * @property int $codigobanda
 * @property string|null $nombre
 * @property float $precios
 * @property string|null $informaciondeperfil
 * @property string $contactos
 * @property string|null $valoraciones
 * @property string|null $codigousuario
 * @property string|null $imagen
 *
 * @property Alquilan $alquilan
 * @property Usuarios $codigousuario0
 */
class modelobandas extends \yii\db\ActiveRecord
{
    
    public $file;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bandas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precios'], 'required'],
            [['precios'], 'number'],
            [['codigousuario'], 'integer'],
            [['nombre', 'informaciondeperfil', 'valoraciones', 'imagen', 'contactos'], 'string', 'max' => 680],
            [['codigousuario'], 'unique'],
            [['codigousuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['codigousuario' => 'codigousuario']],
            [['file'], 'file', 'extensions' => 'jpg, png, jpeg, gif', 'maxFiles' => '1'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigobanda' => 'Codigobanda',
            'nombre' => 'Nombre',
            'precios' => 'Precios',
            'informaciondeperfil' => 'Informaciondeperfil',
            'valoraciones' => 'Valoraciones',
            'codigousuario' => 'Codigousuario',
            'imagen' => 'Imagen',
            'contactos' => 'Contactos',
            'file' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Alquilan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquilan()
    {
        return $this->hasOne(Alquilan::class, ['codigobanda' => 'codigobanda']);
    }

    /**
     * Gets query for [[Codigousuario0]].
     *
     * @return \yii\db\ActiveQuery
    */
    public function getCodigousuario0()
    {
        return $this->hasOne(Usuarios::class, ['codigousuario' => 'codigousuario']);
    }

    /**
     * After find method to format the 'precios' attribute.
     */
    public function afterFind()
    {
        parent::afterFind();

        // Format 'precios' attribute using number_format
    $this->precios = number_format($this->precios, 0, '.', ',');
    }
}
