<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventos".
 *
 * @property int $codigoevento
 * @property string|null $nombre
 * @property string|null $informacion
 *
 * @property Alquilan $alquilan
 * @property Contratan $contratan
 * @property Patrocinan $patrocinan
 * @property string|null $imagen
 */
class modeloeventos extends \yii\db\ActiveRecord
{
    
        public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['informacion'], 'string'],
            [['nombre', 'imagen'], 'string', 'max' => 255],
            
            [['file'], 'file', 'extensions' => 'jpg, png, jpeg, gif', 'maxFiles' => '1'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoevento' => 'Codigoevento',
            'nombre' => 'Nombre',
            'informacion' => 'Informacion',
            'imagen' => 'Imagen',
            'file' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Alquilan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquilan()
    {
        return $this->hasOne(Alquilan::class, ['codigoevento' => 'codigoevento']);
    }

    /**
     * Gets query for [[Contratan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratan()
    {
        return $this->hasOne(Contratan::class, ['codigoevento' => 'codigoevento']);
    }

    /**
     * Gets query for [[Patrocinan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinan()
    {
        return $this->hasOne(Patrocinan::class, ['codigoevento' => 'codigoevento']);
    }
    
    
}
