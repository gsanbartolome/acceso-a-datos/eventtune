<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property string $codigousuario
 * @property string $nombredeusuario
 * @property string|null $informacionpersonal
 *
 * @property Bandas $bandas
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigousuario', 'nombredeusuario'], 'required'],
            [['informacionpersonal'], 'string'],
            [['codigousuario', 'nombredeusuario'], 'string', 'max' => 255],
            [['nombredeusuario'], 'unique'],
            [['codigousuario'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigousuario' => 'Codigousuario',
            'nombredeusuario' => 'Nombredeusuario',
            'informacionpersonal' => 'Informacionpersonal',
        ];
    }

    /**
     * Gets query for [[Bandas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBandas()
    {
        return $this->hasOne(Bandas::class, ['codigousuario' => 'codigousuario']);
    }
}
