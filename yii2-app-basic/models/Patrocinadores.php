<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property int $idpatrocinador
 * @property string|null $nombre
 *
 * @property Patrocinan[] $patrocinans
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpatrocinador' => 'Idpatrocinador',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Patrocinans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinans()
    {
        return $this->hasMany(Patrocinan::class, ['idpatrocinador' => 'idpatrocinador']);
    }
}
