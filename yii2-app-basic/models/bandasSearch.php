<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\modelobandas;

/**
 * bandasSearch represents the model behind the search form of `app\models\modelobandas`.
 */
class bandasSearch extends modelobandas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigobanda', 'valoraciones', 'codigousuario'], 'integer'],
            [['nombre', 'precios', 'informaciondeperfil'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = modelobandas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigobanda' => $this->codigobanda,
            'valoraciones' => $this->valoraciones,
            'codigousuario' => $this->codigousuario,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'informaciondeperfil', $this->informaciondeperfil]);

        // Filter by keyword
        if (!empty($params['keyword'])) {
            $query->andFilterWhere(['like', 'informaciondeperfil', $params['keyword']]);
        }

        // Filter by price range
        if (!empty($params['minPrice'])) {
            $query->andWhere(['>=', 'precios', $params['minPrice']]);
        }
        if (!empty($params['maxPrice'])) {
            $query->andWhere(['<=', 'precios', $params['maxPrice']]);
        }

        return $dataProvider;
    }
}
