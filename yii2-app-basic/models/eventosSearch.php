<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\modeloeventos;

/**
 * eventosSearch represents the model behind the search form of `app\models\modeloeventos`.
 */
class eventosSearch extends modeloeventos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoevento'], 'integer'],
            [['nombre', 'informacion', 'imagen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = modeloeventos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codigoevento' => $this->codigoevento,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'informacion', $this->informacion])
            ->andFilterWhere(['like', 'imagen', $this->imagen]);

        // Filter by keyword
        if (!empty($params['keyword'])) {
            $query->andFilterWhere(['like', 'nombre', $params['keyword']]);
        }

        return $dataProvider;
    }
}
