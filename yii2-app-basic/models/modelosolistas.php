<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solistas".
 *
 * @property int $codigosolista
 * @property string|null $nombre
 * @property float $precio
 * @property string|null $informaciondeperfil
 * @property string|null $valoraciones
 * @property string|null $contactos
 * @property string|null $imagen
 *
 * @property Contratan $contratan
 */
class modelosolistas extends \yii\db\ActiveRecord
{
    public $file;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'required'],
            [['precio'], 'number'],
            [['informaciondeperfil', 'valoraciones', 'contactos'], 'string'],
            [['nombre', 'imagen'], 'string', 'max' => 680],
            [['file'], 'file', 'extensions' => 'jpg, png, jpeg, gif', 'maxFiles' => '1'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigosolista' => 'Codigosolista',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'informaciondeperfil' => 'Informacion de Perfil',
            'valoraciones' => 'Valoraciones',
            'contactos' => 'Contactos',
            'imagen' => 'Imagen',
            'file' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Contratan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratan()
    {
        return $this->hasOne(Contratan::class, ['codigosolista' => 'codigosolista']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */

    /**
     * After find method to format the 'precio' attribute.
     */
    public function afterFind()
    {
        parent::afterFind();

        // Format 'precio' attribute using number_format
        $this->precio = number_format($this->precio, 0, '.', ',');
    }
}
