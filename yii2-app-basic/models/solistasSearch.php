<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\modelosolistas;

/**
 * solistasSearch represents the model behind the search form of `app\models\modelosolistas`.
 */
class solistasSearch extends modelosolistas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigosolista'], 'integer'],
            [['nombre', 'valoraciones', 'contactos', 'informaciondeperfil'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = modelosolistas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'codigosolista' => $this->codigosolista,
            'precio' => $this->precio,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'valoraciones', $this->valoraciones])
            ->andFilterWhere(['like', 'contactos', $this->contactos])
            ->andFilterWhere(['like', 'informaciondeperfil', $this->informaciondeperfil]);

        // Filter by keyword
        if (!empty($params['keyword'])) {
            $query->andFilterWhere(['like', 'informaciondeperfil', $params['keyword']]);
        }

        // Filter by price range
        if (!empty($params['minPrice'])) {
            $query->andWhere(['>=', 'precio', $params['minPrice']]);
        }
        if (!empty($params['maxPrice'])) {
            $query->andWhere(['<=', 'precio', $params['maxPrice']]);
        }

        return $dataProvider;
    }
}
