﻿-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-06-2024 a las 23:52:50
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eventtune`
--
DROP DATABASE IF EXISTS eventtune;
CREATE DATABASE eventtune;
USE eventtune;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquilan`
--

CREATE TABLE `alquilan` (
  `codigoalquilan` int(11) NOT NULL,
  `codigoevento` int(11) DEFAULT NULL,
  `codigobanda` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bandas`
--

CREATE TABLE `bandas` (
  `codigobanda` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precios` decimal(10,2) NOT NULL,
  `informaciondeperfil` varchar(3000) DEFAULT NULL,
  `valoraciones` varchar(255) DEFAULT NULL,
  `codigousuario` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `contactos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bandas`
--

INSERT INTO `bandas` (`codigobanda`, `nombre`, `precios`, `informaciondeperfil`, `valoraciones`, `codigousuario`, `imagen`, `contactos`) VALUES
(81, 'D.K. JOHN BAND', '1200.00', 'Repertorio: Rock de los 70\'s, 80\'s y 90\'s\r\nEn español y en ingles\r\nConciertos de dos horas\r\nSi se desea escoger un repertorio concreto consultar\r\nQuinteto', NULL, NULL, 'UKFiHw8iyKCFMXQq82OHO3gKxXZVN-Wp.jpg', 'risanbara@gmail.com\r\ntelefono: 6461854536\r\n'),
(82, 'LOS GARRAPATEROS', '750.00', 'Flamenco Fusión:\r\nRumbas, versiones propias\r\nNo tenemos problemas con desplazarnos de ser necesario\r\nsolista, dos guitarras, percusion', 'increibles', NULL, '-bLZ7MSRe0fK9YVI9oauaiREYaVYBDqj.jpg', 'flamencorico@hotmail.com\r\n7774966565'),
(83, 'KAISER SOSE', '850.00', 'TECNO FUNKY PUNKA BILLY\r\nGenero fusion, texmex, cuarteto\r\ncon vocalista\r\nversiones propias', NULL, NULL, 'OSkARxB9RE1i3i7GUAsot2OxIsx96vlv.jpg', 'Kaisersose@gmail.com\r\ntelefono: 347838924\r\n@kaisersose45 (En Instagram)'),
(84, 'LOS ROMANTICOS Y MR SMITH', '850.00', 'Pop Español de los 60\'s\r\nCover con armonias vocales\r\nDespedidas de solteros, y fiestas privadas\r\n\r\nSolo en locales de interior', NULL, NULL, 'bN8fudgDRfq6qsSeP0uY-O6O19axJhhw.jpg', 'mrsmith@wanadoo.es\r\ntelefono: 942365365'),
(85, 'maneros y tony', '0.00', 'Musica disco\r\nEstetica John travolta\r\nMusica de los Bee Gees, Crussaders, Earth Wind and Fire\r\nPrecio negociable\r\nRepertorio, 1:40 hrs', 'Maravilloso tengo ganas de volver a contratarlos', NULL, 'cF9gKdt5mVJmEa-x7Blkz4oFjvEHyLBL.jpg', 'Tonymanera80@gmail.com\r\n@ManerosAlaVida'),
(86, 'TRANKITRON', '800.00', 'BLUES\r\nRepertorio: BB KING, MUDDIE WATERS, ERIC CLAPTON\r\n\r\nVERSIONES\r\nGuitarra y Armonica\r\nFacilidad de desplazamiento', '5 ESTRELLAS\r\nYA LES HABIAMOS CONTRATADO EN OTRA OCASION Y NUNCA DEFRAUDAN\r\nTRANKI Y JAVI SON DEL O MEJOR', NULL, 'Ie5fdssjBIu-BK7c47ouv0-jP1afqCiH.jpg', 'Trankitro@hotmail.com\r\nTelefono: 6664512406'),
(87, 'SCHOLLA DUO', '650.00', 'Genero Rock Chellos\r\nversiones de metallica, led zeppelin, etc....\r\n\r\nDos profesores de conservatorios unidos por una misma pasion', NULL, NULL, 'ZFNDyeLLz0rXmDdsciMd09oAqgCVIdC3.jpg', 'cellocello@gmail.com\r\ntelefono: 3829439242'),
(88, 'LIRICAL ARS', '950.00', 'Musica Lirica, acompañados de bases pre-grabadas\r\nrepertorio de Areas conocidas\r\nOpera Zarcuela....\r\n\r\n', NULL, NULL, 'W6yxYS44WK23IjwxSgrDw42R6BAPs-LD.jpg', 'Liricalar18000@gmail.com\r\nLiricasatope (En Youtube)'),
(89, 'JHON AND MARY HIGHS', '400.00', 'Flauta y violin clasico\r\nun duo dinamico que no defrauda\r\nnos desplazamos de ser necesario\r\n\r\nMozarte, Chopin, Piazzolla....', 'Muy profesionales!! llegaron con mucho tiempo de antelación y muy educados\n9/10', NULL, '0FrbVA0VVfWLGpiezgzjgBMFOIqDgaFH.jpg', 'JDM HIGH (En youtube)'),
(90, 'OSORIO, MARIA Y SU RABEL', '250.00', 'Musica Folk de Cantabria, con acompañamiento de Rabel y pandereta, dos voces, coplas y canciones picaras\r\nestetica tradicional\r\n\r\nLa foto es de nuestro abuelo, con el rabel original, que fallecio a los 105 años, larga tradicion\r\nconstructor de Rabeles\r\nTambien hacemos tallar de construccion de eventos\r\n\r\nsolo tocamos en cantabria y en alrededores', 'Yo soy el chaval de al lado', NULL, 'nLLxMFsh4h3k0rOUsIH0wHuSGr2zVsKD.jpg', 'telefono: 3893824383\r\nrabelmaravilla78@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratan`
--

CREATE TABLE `contratan` (
  `codigocontratan` int(11) NOT NULL,
  `codigoevento` int(11) DEFAULT NULL,
  `codigosolista` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `codigoevento` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `informacion` text,
  `contacto` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`codigoevento`, `nombre`, `informacion`, `contacto`, `imagen`) VALUES
(13, 'BODA EN POLIENTES', 'El 8 de septiembre me caso en una casa en polientes y me gustaria encontrar un grupo de rock y cantante y violin de clasica, el evento seria por la tarde interesados contactar con nosotros antes del 15 de Junio Somos luis y Maria, este es nuestro telefono (6546833 el de Maria), (6598777 el mio) Preferentemente llamarnos por las tardes', NULL, '7HFjIz4cYEGktKipvGUMrEo1nu_TRSYr.jpg'),
(14, 'CUMPLEAÑOS INFANTIL', 'Necesitamos un cantante o grupo que sepa cantar acnciones infantiles, divertidas y si ademas viene acompañado de algun payaso o un monitos divertido o que realice juegos muchisimo mejor Me llamo Teresa y mi numero de telefono es (734274832) Llamarme por las mañanas si es posible', NULL, 'vwhjqoUMQiohDDhwttYGYTqEe5Mwqd2k.jpg'),
(15, 'ANIVERSARIO DE DEFUNCION', 'el 10 de noviembre nos gustaria realizar una Misa por nuestra querida Luisa, nos gustaria contratar algun grupo de violines o flautas y violin, etc... Algo clasico Me llamo Pedro mi email es aupaelrazing@gmail.com Podeis enviarme alguna grabacion o algun trabajo vuestro', NULL, 'SqnOy5s28QJzfiaEDjcmi78CkhK4jWKo.jpg'),
(16, 'FIESTON CON AMIGOS', 'Nos gustaria encontrar un grupo divertido para un reencuentro de viejos amigos, lo haremos en pedreña el ultimo fin de semana de agosto, porfavor enviadnos grabaciones o algun trabajo vuestro Mi telefono es (859459343)', NULL, 'qde93SPmgf4dChJseKQgCVTsnS_w4mWH.jpg'),
(17, 'PARA LAS FIESTAS DE UN PUEBLO', 'Soy pilar la alcaldesa de Bruje, un pueblecito de Asturias necesitaria alguna banda con canciones clasicas y bailables para las fiestas de nuestro pueblo que son el 13 de septiembre, enviar propuestas al correo de la alcaldia PilarAyuntamientodebruje@gmail.com', NULL, 'Y9WV2dhuJLecUolV-6e9TNBx9MJrCH1W.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinadores`
--

CREATE TABLE `patrocinadores` (
  `idpatrocinador` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinan`
--

CREATE TABLE `patrocinan` (
  `codigopatrocinan` int(11) NOT NULL,
  `codigoevento` int(11) DEFAULT NULL,
  `idpatrocinador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solistas`
--

CREATE TABLE `solistas` (
  `codigosolista` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `precio` decimal(10,2) NOT NULL,
  `informaciondeperfil` text,
  `valoraciones` text,
  `contactos` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `solistas`
--

INSERT INTO `solistas` (`codigosolista`, `nombre`, `precio`, `informaciondeperfil`, `valoraciones`, `contactos`, `imagen`) VALUES
(2, 'DANDY PANDIE', '500.00', 'Reggaeton, musica de DJ\r\nEstetica urbana latina\r\nversiones propias\r\n\r\n', '7/10\nun poco cutres >:(\nNi saludaron, llegaron tocaron y se fueron no se enrollaron nada\n5/10', 'DaddyKKK@gmail.com', '1-rn1UdMbFs-W7QPBdU7J25uP8F1KcWC.jpg'),
(3, 'LADY SUNDEY', '470.00', 'Repertorio musica electronica actual\r\nMezclas, Tecno Mix para grandes eventos\r\n\r\n', NULL, 'Sundayla@hotmail.com\r\ntelefono: 64646464649', 'm8i2GFvdJD4i5bAhYCKP8hjlKmp0-Kc_.jpg'),
(4, 'ANTONIO LEPERA', '300.00', 'Voz y Guitarra, boleros, canciones de ayer y hoy\r\nRepertorio: Gardel, Nino Bravo, Jose Luis Perales, Rafael....', NULL, 'LePera2004@gmail.com\r\nTelefono: 384729238', 'K88sbi64zBLd9Ng4ZEzWVoSWNk1XIvYD.jpg'),
(5, 'RENE BARBUE', '800.00', 'Piano clasico\r\nRene y su piano interpretan Arias Clasicas de famosas zarzuelas\r\n\r\nVersiones por encargo', '9/10\r\nMuy profesional, nos encanto, le enviamos un repertorio con 12 temas para nuestra boda, un mes antes del evento y todas las interpreto de maravilla recomiendo poneros en contacto con el', 'Renireno1800@gmail.com\r\nPiano Renero (En youtube)\r\n@Piareno (Instagram)', 'hyHmaC8T8prm0GI7j7cm88bO1Wpq0-m3.jpg'),
(6, 'DICK ROMAN', '500.00', 'Cantante Americano\r\nVoz y banjo, exitos del country americano, musica del sur de estados unidos, winscowsins', 'Divertidisimo, muy enrollado, se quedo mas tiempo, y nos hizo a todos bailar', 'Telefono: 43832934\r\nCountryGod (En youtube me podeis encontrar con ese nombre)', 'Jx3lIi8P1q6KlGbdaultxk4l_i_NsZZs.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `codigousuario` varchar(255) NOT NULL,
  `nombredeusuario` varchar(255) NOT NULL,
  `informacionpersonal` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquilan`
--
ALTER TABLE `alquilan`
  ADD PRIMARY KEY (`codigoalquilan`),
  ADD UNIQUE KEY `uk_eventos` (`codigoevento`),
  ADD UNIQUE KEY `uk_banda` (`codigobanda`);

--
-- Indices de la tabla `bandas`
--
ALTER TABLE `bandas`
  ADD PRIMARY KEY (`codigobanda`),
  ADD UNIQUE KEY `uk_usuario` (`codigousuario`);

--
-- Indices de la tabla `contratan`
--
ALTER TABLE `contratan`
  ADD PRIMARY KEY (`codigocontratan`),
  ADD UNIQUE KEY `uk_eventos` (`codigoevento`),
  ADD UNIQUE KEY `uk_solista` (`codigosolista`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`codigoevento`);

--
-- Indices de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  ADD PRIMARY KEY (`idpatrocinador`);

--
-- Indices de la tabla `patrocinan`
--
ALTER TABLE `patrocinan`
  ADD PRIMARY KEY (`codigopatrocinan`),
  ADD UNIQUE KEY `uk_patrocinador` (`codigopatrocinan`),
  ADD UNIQUE KEY `uk_eventos` (`codigoevento`),
  ADD KEY `fk_patrocinan_patrocinadores` (`idpatrocinador`);

--
-- Indices de la tabla `solistas`
--
ALTER TABLE `solistas`
  ADD PRIMARY KEY (`codigosolista`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigousuario`),
  ADD UNIQUE KEY `uk_usuario_nombreusuario` (`nombredeusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquilan`
--
ALTER TABLE `alquilan`
  MODIFY `codigoalquilan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bandas`
--
ALTER TABLE `bandas`
  MODIFY `codigobanda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT de la tabla `contratan`
--
ALTER TABLE `contratan`
  MODIFY `codigocontratan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `codigoevento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  MODIFY `idpatrocinador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `patrocinan`
--
ALTER TABLE `patrocinan`
  MODIFY `codigopatrocinan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `solistas`
--
ALTER TABLE `solistas`
  MODIFY `codigosolista` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alquilan`
--
ALTER TABLE `alquilan`
  ADD CONSTRAINT `fk_alquilan_bandas` FOREIGN KEY (`codigobanda`) REFERENCES `bandas` (`codigobanda`),
  ADD CONSTRAINT `fk_alquilan_eventos` FOREIGN KEY (`codigoevento`) REFERENCES `eventos` (`codigoevento`);

--
-- Filtros para la tabla `bandas`
--
ALTER TABLE `bandas`
  ADD CONSTRAINT `fk_banda_usuario` FOREIGN KEY (`codigousuario`) REFERENCES `usuarios` (`codigousuario`);

--
-- Filtros para la tabla `contratan`
--
ALTER TABLE `contratan`
  ADD CONSTRAINT `fk_alquilan_solistas` FOREIGN KEY (`codigosolista`) REFERENCES `solistas` (`codigosolista`),
  ADD CONSTRAINT `fk_contratan_eventos` FOREIGN KEY (`codigoevento`) REFERENCES `eventos` (`codigoevento`);

--
-- Filtros para la tabla `patrocinan`
--
ALTER TABLE `patrocinan`
  ADD CONSTRAINT `fk_patrocinan_eventos` FOREIGN KEY (`codigoevento`) REFERENCES `eventos` (`codigoevento`),
  ADD CONSTRAINT `fk_patrocinan_patrocinadores` FOREIGN KEY (`idpatrocinador`) REFERENCES `patrocinadores` (`idpatrocinador`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
